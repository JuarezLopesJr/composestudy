package com.example.composestudy

import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.example.composestudy.ui.theme.ComposeStudyTheme
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@ExperimentalAnimationApi
@ExperimentalComposeUiApi
@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {

    /* test only the layout without the need of a activity */
    @get:Rule
    val composeTestRule = createComposeRule()

    /* only if the test requires a specific activity */
    @ExperimentalMaterialApi
    @get:Rule
    val androidTestRule =
        createAndroidComposeRule(MainActivity::class.java)

    /* to run before each test is initiated */
    @Before
    fun setup() {
        composeTestRule.setContent {
            ComposeStudyTheme {
                // define here the initial state/composable
            }
        }
    }

    @ExperimentalMaterialApi
    @Test
    fun useAppContext() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        assertEquals("com.example.composestudy", appContext.packageName)

        androidTestRule.activity.getString(R.string.app_name)
    }
}