package com.example.composestudy

import android.os.Build
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.AlertDialog
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.onSizeChanged
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.unit.dp
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.filters.SdkSuppress
//import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withTimeout
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@LargeTest
@RunWith(AndroidJUnit4::class)
@SdkSuppress(minSdkVersion = Build.VERSION_CODES.P)
class AlertDialogTest {

    @get:Rule
    val rule = createComposeRule()

    @Test
    fun alertDialogDoesNotConsumeFullScreenWidth() {
        val dialogWidthCh = Channel<Int>(Channel.CONFLATED)

        var screenWidth by mutableStateOf(0)

        rule.setContent {
            val context = LocalContext.current

            val density = LocalDensity.current

            val resScrWidth = context.resources.configuration.screenWidthDp
            with(density) { screenWidth = resScrWidth.dp.roundToPx() }

            AlertDialog(
                onDismissRequest = {},
                modifier = Modifier
                    .onSizeChanged {
                        dialogWidthCh.trySend(it.width)
                    }
                    .fillMaxWidth(),
                title = { Text(text = "Title") },
                text = {
                    Text(
                        "Mussum Ipsum, o melhor lorem ipsum do mundis."
                    )
                },
                confirmButton = {
                    TextButton(onClick = {}) {
                        Text(text = "Confirm")
                    }
                },
                dismissButton = {
                    TextButton(onClick = {}) {
                        Text(text = "Dismiss")
                    }
                },
                backgroundColor = Color.Yellow,
                contentColor = Color.Red
            )
        }

        runBlocking {
            val dialogWidth = withTimeout(5_000) { dialogWidthCh.receive() }
//            assertThat(dialogWidth).isLessThan(screenWidth)
        }
    }
}