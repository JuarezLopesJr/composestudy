package com.example.composestudy

import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithContentDescription
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.composestudy.view.NavigationCompose
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class NavHostTest {
    @get:Rule
    val composeTestRule = createComposeRule()
    lateinit var navController: NavHostController

    @Before
    fun setupNavHost() {
        composeTestRule.setContent {
            navController = rememberNavController()
            NavigationCompose(navController = navController)
        }
    }

    @Test
    fun mainNavHost() {
        composeTestRule.onNodeWithContentDescription("main_screen")
            .assertIsDisplayed()
    }
}