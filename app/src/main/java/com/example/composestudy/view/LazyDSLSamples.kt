@file:Suppress("unused", "UNUSED_PARAMETER")

package com.example.composestudy.view

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import com.example.composestudy.ui.theme.TextWhite

@ExperimentalFoundationApi
@Composable
fun ItemPlacementAnimationCompose() {
    var list by remember { mutableStateOf(listOf("A", "B", "C", "1", "2", "3")) }

    LazyColumn(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        items(items = list, key = { it }) {
            Text(
                text = "Item $it",
                modifier = Modifier.animateItemPlacement(),
                color = TextWhite
            )

            Spacer(modifier = Modifier.height(16.dp))
        }

        item {
            Button(onClick = { list = list.shuffled() }) {
                Text(text = "Shuffle")
            }
        }
    }
}

@ExperimentalFoundationApi
@Composable
fun StickyHeaderSample() {
    val sections = listOf("A", "B", "C", "D", "E", "F", "G")

    LazyColumn(contentPadding = PaddingValues(all = 6.dp)) {
        sections.forEach { section ->
            stickyHeader {
                Text(
                    text = "Section $section", modifier = Modifier
                        .fillMaxWidth()
                        .background(Color.Green)
                        .padding(all = 8.dp),
                    color = Color.Black
                )
            }

            items(10) {
                Text(
                    text = "Item $it from section $section",
                    color = TextWhite
                )
            }
        }
    }
}

@Composable
fun UsingListScrollPositionInCompositionSample() {
    val listState = rememberLazyListState()
    val isAtTop by remember {
        derivedStateOf {
            listState.firstVisibleItemIndex == 0 && listState.firstVisibleItemScrollOffset == 0
        }
    }

    if (!isAtTop) ScrollToTopButton(listState = listState)
}

@Composable
private fun ScrollToTopButton(listState: LazyListState) {
}