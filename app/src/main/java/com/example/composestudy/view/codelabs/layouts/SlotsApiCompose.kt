package com.example.composestudy.view.codelabs.layouts

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material.icons.filled.MoreVert
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.composestudy.ui.theme.ComposeStudyTheme

@Composable
fun SlotsApiCompose() {
    Scaffold(
        topBar = {
            TopAppBar(
                title = {
                    Text(
                        text = "Slots Api Compose",
                    )
                },
                actions = {
                    IconButton(onClick = { }) {
                        Icon(
                            imageVector = Icons.Default.Favorite,
                            contentDescription = null
                        )
                    }
                    IconButton(onClick = { }) {
                        Icon(
                            imageVector = Icons.Default.MoreVert,
                            contentDescription = null
                        )
                    }
                }
            )
        }
    ) {
        BodyContent(Modifier.padding(16.dp))
    }
}

@Composable
fun BodyContent(modifier: Modifier = Modifier) {
    /* modifiers will be set/applied in call site
       When there is no available chaining method, you can use .then()
       modifier = Modifier.then(modifier)
    */
    Column(modifier = modifier) {
        Text(text = "slot text test 1")
        Text(text = "slot text test 2")
    }
}

@Preview
@Composable
fun SlotsApiPreview() {
    ComposeStudyTheme(darkTheme = true) {
        SlotsApiCompose()
    }
}