package com.example.composestudy.view

import android.content.Context
import androidx.compose.foundation.clickable
import androidx.compose.foundation.gestures.forEachGesture
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Divider
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.ModalBottomSheetLayout
import androidx.compose.material.ModalBottomSheetValue
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Text
import androidx.compose.material.TextFieldColors
import androidx.compose.material.TextFieldDefaults
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowDropDown
import androidx.compose.material.rememberModalBottomSheetState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.input.pointer.PointerEvent
import androidx.compose.ui.input.pointer.PointerEventPass
import androidx.compose.ui.input.pointer.changedToUp
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.onClick
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.io.IOException
import kotlinx.coroutines.coroutineScope

data class Country(
    val code: String,
    val name: String,
    val dialCode: String,
)

fun countryList(context: Context): MutableList<Country> {
    val jsonFileString = getJsonDataFromFile(context, "countries.json")
    val type = object : TypeToken<List<Country>>() {}.type

    return Gson().fromJson(jsonFileString, type)
}

fun getJsonDataFromFile(
    context: Context,
    fileName: String
): String? {
    val jsonString: String

    try {
        jsonString = context.assets.open(fileName).bufferedReader().use {
            it.readText()
        }
    } catch (e: IOException) {
        return null
    }

    return jsonString
}

fun localeToEmoji(
    countryCode: String
): String {
    val firstLetter = Character.codePointAt(countryCode, 0) - 0x41 + 0x1F1E6
    val secondLetter = Character.codePointAt(countryCode, 1) - 0x41 + 0x1F1E6

    return String(Character.toChars(firstLetter)) + String(Character.toChars(secondLetter))
}

@ExperimentalMaterialApi
@Composable
fun CountryList() {
    Box {
        var expanded by remember { mutableStateOf(false) }

        var selectedCountry by remember { mutableStateOf<Country?>(null) }

        CountryPickerBottomSheet(
            title = {
                Text(
                    text = "Select country",
                    textAlign = TextAlign.Center,
                    fontWeight = FontWeight.Bold,
                    fontSize = 20.sp,
                    modifier = Modifier.fillMaxWidth()
                )
            },
            onItemSelected = {
                selectedCountry = it
                expanded = false
            },
            show = expanded,
            onDismissRequest = { expanded = false })
        {
            CountryTextField(
                label = "Select Country",
                modifier = Modifier
                    .padding(top = 50.dp)
                    .align(Alignment.TopCenter),
                expanded = expanded,
                selectedCountry = selectedCountry
            ) {
                expanded = !expanded
            }
        }
    }
}

@Composable
fun CountryTextField(
    modifier: Modifier = Modifier,
    label: String = "",
    isError: Boolean = false,
    shape: Shape = MaterialTheme.shapes.small,
    expanded: Boolean = false,
    selectedCountry: Country? = null,
    colors: TextFieldColors = TextFieldDefaults.outlinedTextFieldColors(),
    onExpandedChange: () -> Unit
) {
    OutlinedTextField(
        value = if (selectedCountry == null) ""
        else "${(selectedCountry.dialCode)} ${selectedCountry.name}",
        onValueChange = {},
        modifier = modifier.expandable(
            menuLabel = label,
            onExpandedChange = onExpandedChange
        ),
        readOnly = true,
        isError = isError,
        label = {
            Text(text = label)
        },
        colors = colors,
        shape = shape,
        trailingIcon = {
            Icon(
                imageVector = Icons.Default.ArrowDropDown,
                contentDescription = null,
                modifier = Modifier.rotate(if (expanded) 180f else 0f)
            )
        }
    )
}

fun Modifier.expandable(
    onExpandedChange: () -> Unit,
    menuLabel: String
) = pointerInput(Unit) {
    forEachGesture {
        coroutineScope {
            awaitPointerEventScope {
                var event: PointerEvent

                do {
                    event = awaitPointerEvent(PointerEventPass.Initial)
                } while (!event.changes.all {
                        it.changedToUp()
                    }
                )
                onExpandedChange.invoke()
            }
        }
    }
}.semantics {
    contentDescription = menuLabel
    onClick {
        onExpandedChange()
        true
    }
}

@ExperimentalMaterialApi
@Composable
fun CountryPickerBottomSheet(
    modifier: Modifier = Modifier,
    title: @Composable () -> Unit,
    show: Boolean,
    onItemSelected: (country: Country) -> Unit,
    onDismissRequest: () -> Unit,
    content: @Composable () -> Unit
) {
    val context = LocalContext.current

    val countries = remember { countryList(context) }

    var selectedCountry by remember { mutableStateOf(countries[0]) }

    val modalBottomSheetState =
        rememberModalBottomSheetState(initialValue = ModalBottomSheetValue.Hidden)

    LaunchedEffect(key1 = show) {
        if (show) modalBottomSheetState.show() else modalBottomSheetState.hide()
    }

    LaunchedEffect(key1 = modalBottomSheetState.currentValue) {
        if (modalBottomSheetState.currentValue == ModalBottomSheetValue.Hidden) {
            onDismissRequest()
        }
    }

    ModalBottomSheetLayout(
        sheetContent = {
            title()
            LazyColumn(
                contentPadding = PaddingValues(16.dp)
            ) {
                items(countries.size) { index ->
                    Row(
                        modifier = modifier
                            .clickable {
                                selectedCountry = countries[index]
                                onItemSelected(selectedCountry)
                            }
                            .padding(10.dp)
                    ) {
                        Text(
                            text = localeToEmoji(countryCode = countries[index].code)
                        )

                        Text(
                            text = countries[index].name,
                            modifier = Modifier
                                .padding(start = 6.dp)
                                .weight(2f)
                        )

                        Text(
                            text = countries[index].dialCode,
                            modifier = Modifier.padding(start = 6.dp)
                        )
                    }

                    Divider(color = Color.LightGray, thickness = 0.5.dp)
                }
            }
        },
        sheetState = modalBottomSheetState,
        sheetShape = RoundedCornerShape(
            topStart = 20.dp, topEnd = 20.dp
        )
    ) {
        content()
    }
}
