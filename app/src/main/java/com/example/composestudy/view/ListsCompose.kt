package com.example.composestudy.view

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.composestudy.ui.theme.ComposeStudyTheme

@Preview(showSystemUi = true, showBackground = true)
@Composable
fun ListsCompose() {
    ComposeStudyTheme(darkTheme = true) {
        /* list with finite number of items, MUST NOT BE LONG
        val scrollState = rememberScrollState()
        Column(modifier = Modifier.verticalScroll(state = scrollState)) {
            for (i in 1..60) {
                Text(
                    text = "Item number: $i",
                    fontSize = 24.sp,
                    textAlign = TextAlign.Center,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(16.dp),
                )
            }
        }*/

        /* for longer lists ALWAYS use LazyColumn */
        LazyColumn {
            /* there's also itemsIndexed() */
            items(6000) {
                Text(
                    text = "Item number: $it",
                    fontSize = 24.sp,
                    textAlign = TextAlign.Center,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(16.dp),
                )
            }
        }
    }
}