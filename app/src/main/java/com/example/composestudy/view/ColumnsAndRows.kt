package com.example.composestudy.view

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp

/*
the arrangement refers to the main axis column is vertical and row horizontal


*/

@Preview(showSystemUi = true, showBackground = true)
@Composable
fun ColumnsAndRowsPreview() {
    Column(
        modifier = Modifier
            .width(60.dp)
            .height(60.dp)
            .background(Color.DarkGray),
        verticalArrangement = Arrangement.SpaceEvenly,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {

        Row(
            Modifier
                .background(Color.Green)
                .fillMaxWidth()
                .border(6.dp, Color.Blue)
                .padding(all = 8.dp)
                .border(6.dp, Color.Magenta)
                .padding(6.dp)
                .border(6.dp, Color.Yellow)
                .padding(9.dp)
                .border(9.dp, Color.Red)
                .padding(6.dp),
            horizontalArrangement = Arrangement.Center,
            verticalAlignment = Alignment.Bottom
        ) {

            Text(// checkout about offset
                text = "Test text"
            )
        }

        Text(text = "Test text")
        Text(text = "Test text")
    }
}