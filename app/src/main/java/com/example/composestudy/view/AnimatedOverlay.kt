package com.example.composestudy.view

import androidx.annotation.DrawableRes
import androidx.compose.animation.core.animateFloat
import androidx.compose.animation.core.updateTransition
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Android
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawWithContent
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.center
import androidx.compose.ui.graphics.BlendMode
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.graphics.drawscope.DrawScope
import androidx.compose.ui.graphics.drawscope.rotate
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.example.composestudy.R

interface AnimatedOverlay {
    fun drawOverlay(drawScope: DrawScope)
}

class StrikeThruOverlay(
    private val color: Color = Color.Unspecified,
    private val widthDp: Dp = 4.dp,
    private val getProgress: () -> Float
) : AnimatedOverlay {
    override fun drawOverlay(drawScope: DrawScope) {
        with(drawScope) {
            val width = widthDp.toPx()
            val halfWidth = width / 2
            val progressHeight = size.height * getProgress()

            rotate(-45f) {
                drawLine(
                    color = color,
                    start = Offset(size.center.x + halfWidth, 0f),
                    end = Offset(size.center.x + halfWidth, progressHeight),
                    strokeWidth = width,
                    blendMode = BlendMode.Clear
                )

                drawLine(
                    color = color,
                    start = Offset(size.center.x - halfWidth, 0f),
                    end = Offset(size.center.x - halfWidth, progressHeight),
                    strokeWidth = width
                )
            }
        }
    }
}

fun Modifier.animatedOverlay(animatedOverlay: AnimatedOverlay) = this.then(
    /* It's more performant to modify alpha in a graphics layer than using Modifier.alpha() */
    Modifier
        .graphicsLayer {
            alpha = 0.99f
        }
        .drawWithContent {
            drawContent()
            animatedOverlay.drawOverlay(this)
        }
)

@Composable
fun StrikeThruIcon(
    modifier: Modifier = Modifier,
    imageVector: ImageVector = Icons.Outlined.Android
) {
    Surface(
        color = MaterialTheme.colors.background
    ) {
        var enabled by remember { mutableStateOf(false) }

        val transition = updateTransition(targetState = enabled, label = null)

        val progress by transition.animateFloat(label = "Progress") { state ->
            if (state) 1f else 0f
        }

        val overlay = StrikeThruOverlay(color = Color.Green, getProgress = { progress })

        Icon(
            imageVector = imageVector,
            contentDescription = null,
            tint = Color.Green,
            modifier = modifier
                .clickable { enabled = !enabled }
                .padding(8.dp)
                .animatedOverlay(overlay)
                .padding(12.dp)
                .size(52.dp)
        )
    }
}

@Composable
fun StrikeThruImage(
    modifier: Modifier = Modifier,
    @DrawableRes drawableId: Int = R.drawable.ic_videocam
) {
    Surface(
        color = MaterialTheme.colors.background
    ) {
        var enabled by remember { mutableStateOf(false) }

        val transition = updateTransition(targetState = enabled, label = null)

        val progress by transition.animateFloat(label = "Progress") { state ->
            if (state) 1f else 0f
        }

        val overlay = StrikeThruOverlay(color = Color.Green, getProgress = { progress })

        Image(
            painter = painterResource(id = drawableId),
            contentDescription = null,
            colorFilter = ColorFilter.tint(Color.Green),
            modifier = modifier
                .clickable { enabled = !enabled }
                .padding(8.dp)
                .animatedOverlay(overlay)
                .padding(12.dp)
                .size(52.dp)
        )
    }
}