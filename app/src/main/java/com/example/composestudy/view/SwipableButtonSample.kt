package com.example.composestudy.view

import androidx.compose.animation.Crossfade
import androidx.compose.foundation.background
import androidx.compose.foundation.gestures.Orientation
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.FractionalThreshold
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowForward
import androidx.compose.material.icons.filled.Done
import androidx.compose.material.rememberSwipeableState
import androidx.compose.material.swipeable
import androidx.compose.runtime.Composable
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.composestudy.ui.theme.GradientTwo
import com.example.composestudy.ui.theme.Navy
import com.example.composestudy.ui.theme.OrangeYellow1
import com.example.composestudy.ui.theme.Teal200
import kotlin.math.roundToInt

enum class ConfirmationState {
    DEFAULT,
    CONFIRMED
}

@Composable
private fun DraggableControl(
    modifier: Modifier = Modifier,
    progress: Float
) {
    Box(
        modifier = modifier
            .padding(4.dp)
            .shadow(
                elevation = 2.dp,
                shape = CircleShape,
                clip = false
            )
            .background(OrangeYellow1, CircleShape),
        contentAlignment = Alignment.Center
    ) {
        val isConfirmed by derivedStateOf { progress >= 0.8f }

        Crossfade(targetState = isConfirmed) {
            if (it) {
                Icon(
                    imageVector = Icons.Default.Done,
                    contentDescription = null,
                    tint = Navy
                )
            } else {
                Icon(
                    imageVector = Icons.Default.ArrowForward,
                    contentDescription = null,
                    tint = Navy
                )
            }
        }
    }
}

@ExperimentalMaterialApi
@Composable
fun SwipableButtonCompose(modifier: Modifier = Modifier) {
    val width = 350.dp

    val dragSize = 50.dp

    val swipeableState =
        rememberSwipeableState(initialValue = ConfirmationState.DEFAULT)

    val sizePx = with(LocalDensity.current) {
        (width - dragSize).toPx()
    }

    val anchors =
        mapOf(0f to ConfirmationState.DEFAULT, sizePx to ConfirmationState.CONFIRMED)

    val progress by derivedStateOf {
        if (swipeableState.offset.value == 0f) 0f else swipeableState.offset.value / sizePx
    }

    Box(modifier = modifier
        .width(width)
        .swipeable(
            state = swipeableState,
            anchors = anchors,
            orientation = Orientation.Horizontal,
            thresholds = { _, _ -> FractionalThreshold(0.5f) }
        )
        .background(Teal200, RoundedCornerShape(dragSize))
    ) {
        Column(
            modifier = Modifier
                .align(Alignment.Center)
                .alpha(1f - progress),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center
        ) {
            Text(
                text = "Unlock phone",
                color = GradientTwo,
                fontSize = 18.sp
            )

            Text(
                text = "Swipe to confirm",
                color = GradientTwo,
                fontSize = 12.sp
            )
        }

        DraggableControl(
            modifier = Modifier
                .offset {
                    IntOffset(swipeableState.offset.value.roundToInt(), 0)
                }
                .size(dragSize),
            progress = progress
        )
    }
}

