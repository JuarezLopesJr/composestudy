@file:Suppress("unused")
@file:OptIn(ExperimentalMaterialApi::class)

package com.example.composestudy.view

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.width
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.RangeSlider
import androidx.compose.material.Slider
import androidx.compose.material.SliderDefaults
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.VolumeDown
import androidx.compose.material.icons.filled.VolumeUp
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.composestudy.ui.theme.TextWhite

@Preview
@Composable
fun SliderSample() {
    var sliderPosition by remember { mutableStateOf(0f) }
    Column(modifier = Modifier.fillMaxSize()) {
        Text(text = sliderPosition.toString(), color = TextWhite)
        Row(
            modifier = Modifier
                .fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Icon(
                imageVector = Icons.Default.VolumeDown,
                contentDescription = null,
                tint = TextWhite
            )
            Slider(
                modifier = Modifier.width(350.dp),
                value = sliderPosition,
                onValueChange = { sliderPosition = it }
            )
            Icon(imageVector = Icons.Default.VolumeUp, contentDescription = null, tint = TextWhite)
        }
    }
}

@Composable
fun StepsSliderSample() {
    var sliderPosition by remember { mutableStateOf(0f) }
    Text(text = sliderPosition.toString(), color = TextWhite)

    Slider(
        value = sliderPosition,
        onValueChange = { sliderPosition = it },
        valueRange = 0f..100f,
        onValueChangeFinished = {
            /* launch some business logic update with the state you hold
               i.e: viewModel.updateSelectedSliderValue(sliderPosition) */
        },
        steps = 5,
        colors = SliderDefaults.colors(
            thumbColor = MaterialTheme.colors.secondary,
            activeTrackColor = MaterialTheme.colors.secondary
        )
    )
}

@Composable
fun RangerSliderSample() {
    var sliderPosition by remember { mutableStateOf(0f..100f) }
    Text(text = sliderPosition.toString(), color = TextWhite)

    RangeSlider(
        values = sliderPosition,
        onValueChange = { sliderPosition = it },
        steps = 5,
        valueRange = 0f..100f,
        onValueChangeFinished = {
            /* launch some business logic update with the state you hold
               i.e: viewModel.updateSelectedSliderValue(sliderPosition) */
        },
        colors = SliderDefaults.colors(
            thumbColor = MaterialTheme.colors.secondary,
            activeTrackColor = MaterialTheme.colors.secondary
        )
    )
}

@Composable
fun StepRangeSliderSample() {
    var sliderPosition by remember { mutableStateOf(0f..100f) }
    Text(text = sliderPosition.toString(), color = TextWhite)

    RangeSlider(
        values = sliderPosition,
        onValueChange = { sliderPosition = it },
        steps = 5,
        valueRange = 0f..100f,
        onValueChangeFinished = {
            /* launch some business logic update with the state you hold
               i.e: viewModel.updateSelectedSliderValue(sliderPosition) */
        },
        colors = SliderDefaults.colors(
            thumbColor = MaterialTheme.colors.secondary,
            activeTrackColor = MaterialTheme.colors.secondary
        )
    )
}
