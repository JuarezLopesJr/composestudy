package com.example.composestudy.view

import androidx.compose.animation.core.Animatable
import androidx.compose.animation.core.VectorConverter
import androidx.compose.animation.core.tween
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Android
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.layout.onSizeChanged
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.toSize
import kotlinx.coroutines.launch

@Composable
fun AnimationScaleCompose() {
    Column(
        modifier = Modifier.fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        val animatableSize = remember {
            Animatable(Size.Zero, Size.VectorConverter)
        }

        val (containerSize, setContainerSize) = remember {
            mutableStateOf<Size?>(null)
        }

        val (imageSize, setImageSize) = remember {
            mutableStateOf<Size?>(null)
        }

        val density = LocalDensity.current

        val scope = rememberCoroutineScope()

        Button(
            onClick = {
                scope.launch {
                    if (imageSize == null || containerSize == null) return@launch

                    val targetSize =
                        if (animatableSize.value == imageSize) containerSize else imageSize

                    animatableSize.animateTo(
                        targetValue = targetSize,
                        animationSpec = tween(1000)
                    )
                }
            }
        ) {
            Text(text = "Animate size")
        }

        Box(
            modifier = Modifier
                .padding(20.dp)
                .size(300.dp)
                .onSizeChanged {
                    setContainerSize(it.toSize())
                }
        ) {
            Image(
                imageVector = Icons.Default.Android,
                contentDescription = null,
                colorFilter = ColorFilter.tint(Color.Green),
                modifier = Modifier
                    .then(
                        if (animatableSize.value != Size.Zero) {
                            animatableSize.value.run {
                                Modifier.size(
                                    width = with(density) { width.toDp() },
                                    height = with(density) { height.toDp() }
                                )
                            }
                        } else {
                            Modifier
                        }
                    )
                    .onSizeChanged {
                        if (imageSize != null) return@onSizeChanged

                        val size = it.toSize()

                        setImageSize(size)

                        scope.launch {
                            animatableSize.snapTo(size)
                        }
                    }
            )
        }
    }
}