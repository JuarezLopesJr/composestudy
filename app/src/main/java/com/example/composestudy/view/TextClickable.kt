package com.example.composestudy.view

import androidx.compose.foundation.text.ClickableText
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.LocalUriHandler
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.style.TextDecoration

@Composable
fun TextClickableSample() {
    val uriHandler = LocalUriHandler.current
    val tagInfo = "info"
    val annotatedString = buildAnnotatedString {
        val text = "For more info click here"
        append(text = text)

        val start = text.indexOf("here")
        val end = start + 4

        addStyle(
            style = SpanStyle(
                color = MaterialTheme.colors.primary,
                textDecoration = TextDecoration.Underline
            ),
            start = start,
            end = end
        )

        addStringAnnotation(
            tag = tagInfo,
            annotation = "https://debian.org",
            start = start,
            end = end
        )
    }

    ClickableText(text = annotatedString, onClick = { offset ->
        annotatedString.getStringAnnotations(
            tagInfo,
            offset,
            offset
        ).firstOrNull()?.let { string ->
            uriHandler.openUri(string.item)
        }
    })
}