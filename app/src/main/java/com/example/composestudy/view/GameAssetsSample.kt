package com.example.composestudy.view

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.IconButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.composestudy.R

@Preview
@Composable
fun GameAsset() {
    Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.BottomCenter) {
        Image(
            painter = painterResource(id = R.drawable.mountain),
            contentDescription = null,
            contentScale = ContentScale.FillBounds,
            modifier = Modifier.matchParentSize()
        )

        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(bottom = 8.dp),
            horizontalArrangement = Arrangement.SpaceEvenly
        ) {
            IconButton(onClick = { println("Play clicked") }) {
                Image(
                    painter = painterResource(id = R.drawable.play),
                    contentDescription = null
                )
            }

            IconButton(onClick = { println("Pause clicked") }) {
                Image(
                    painter = painterResource(id = R.drawable.pause),
                    contentDescription = null
                )
            }

            IconButton(onClick = { println("Settings clicked") }) {
                Image(
                    painter = painterResource(id = R.drawable.settings),
                    contentDescription = null
                )
            }
        }
    }
}