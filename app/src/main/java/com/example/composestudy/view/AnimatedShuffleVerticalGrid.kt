@file:Suppress("unused")

package com.example.composestudy.view

import androidx.compose.animation.core.Animatable
import androidx.compose.animation.core.AnimationSpec
import androidx.compose.animation.core.AnimationVector2D
import androidx.compose.animation.core.VectorConverter
import androidx.compose.animation.core.tween
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Slider
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.key
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.DpOffset
import androidx.compose.ui.unit.DpSize
import androidx.compose.ui.unit.dp
import kotlin.math.roundToInt
import kotlinx.coroutines.launch

private const val MIN_SIZE = 2
private const val MAX_SIZE = 6

typealias ItemOffset = Animatable<DpOffset, AnimationVector2D>

data class Item(
    val id: Int,
    val color: Color
)

private val colors = listOf(
    Color(0xFFF44336),
    Color(0xFFE91E63),
    Color(0xFF9C27B0),
    Color(0xFF673AB7),
    Color(0xFF3F51B5),
    Color(0xFF2196F3),
    Color(0xFF03A9F4),
    Color(0xFF00BCD4),
    Color(0xFF009688),
    Color(0xFF4CAF50),
    Color(0xFF8BC34A),
    Color(0xFFCDDC39),
    Color(0xFFFFEB3B),
    Color(0xFFFFC107),
    Color(0xFFFF9800),
    Color(0xFFFF5722),
)

fun createItems(count: Int): List<Item> {
    return (1..count).map { Item(it, colors[it % colors.size]) }
}

fun itemOffset(offset: DpOffset): ItemOffset = Animatable(offset, DpOffset.VectorConverter)

@Composable
fun <ITEM, KEY> AnimatedVerticalGrid(
    modifier: Modifier = Modifier,
    items: List<ITEM>,
    itemKey: (ITEM) -> KEY,
    columns: Int,
    rows: Int,
    animationSpec: AnimationSpec<DpOffset> = tween(1000),
    itemContent: @Composable (BoxScope.(ITEM) -> Unit)
) = BoxWithConstraints(modifier) {

    val itemKeys = items.map { itemKey(it) }

    val itemSize = remember(rows, columns) {
        val itemWidth = (maxWidth) / columns
        val itemHeight = (maxHeight) / rows
        DpSize(itemWidth, itemHeight)
    }

    val gridOffsets = remember(rows, columns, itemSize) {
        (0 until rows).map { column ->
            (0 until columns).map { row ->
                DpOffset(
                    x = itemSize.width * row,
                    y = itemSize.height * column,
                )
            }
        }.flatten()
    }

    var itemsOffsets by remember { mutableStateOf(mapOf<KEY, ItemOffset>()) }

    key(itemKeys) {
        itemsOffsets = items.mapIndexed { index, item ->
            val key = itemKey(item)
            key to when {
                itemsOffsets.containsKey(key) -> itemsOffsets.getValue(key)
                else -> itemOffset(gridOffsets[index])
            }
        }.toMap()
    }

    items.forEach { item ->
        val offset = itemsOffsets.getValue(itemKey(item)).value
        Box(
            modifier = Modifier
                .size(itemSize)
                .offset(offset.x, offset.y)
        ) {
            itemContent(item)
        }
    }

    LaunchedEffect(itemKeys) {
        items.forEachIndexed { index, item ->
            val newOffset = gridOffsets[index]
            val itemOffset = itemsOffsets.getValue(itemKey(item))
            launch { itemOffset.animateTo(newOffset, animationSpec) }
        }
    }
}

@Composable
fun AnimatedShuffleVerticalGrid() {
    var columns by rememberSaveable { mutableStateOf((MIN_SIZE + MAX_SIZE) / 2) }
    var rows by rememberSaveable { mutableStateOf((MIN_SIZE + MAX_SIZE) / 2) }
    var items by remember(columns, rows) { mutableStateOf(createItems(columns * rows)) }

    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(Color.White)
    ) {

        AnimatedVerticalGrid(
            items = items,
            itemKey = Item::id,
            columns = columns,
            rows = rows,
            modifier = Modifier
                .fillMaxWidth()
                .weight(1F)
        ) {
            Item(it)
        }

        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(16.dp)
        ) {

            Button(
                onClick = { items = items.shuffled() },
                modifier = Modifier
                    .padding(vertical = 32.dp)
                    .align(Alignment.CenterHorizontally),
            ) {
                Text(text = "Shuffle")
            }

            Sliders(
                label = "Columns",
                value = columns,
                onChanged = { columns = it }
            )
            Sliders(
                label = "Rows",
                value = rows,
                onChanged = { rows = it }
            )
        }
    }
}

@Composable
fun Item(item: Item) {
    Box(
        modifier = Modifier
            .fillMaxSize()
            .padding(2.dp)
            .background(item.color),
        contentAlignment = Alignment.Center
    ) {
        Text(
            text = item.id.toString()
        )
    }
}

@Composable
fun Sliders(
    label: String,
    value: Int,
    onChanged: (Int) -> Unit
) {
    var sliderValue by remember { mutableStateOf(value.toFloat()) }

    Column(
        modifier = Modifier.fillMaxWidth()
    ) {
        Text(text = "$label: $value", color = MaterialTheme.colors.onSurface)

        Slider(
            value = sliderValue,
            valueRange = MIN_SIZE.toFloat()..MAX_SIZE.toFloat(),
            steps = MAX_SIZE - MIN_SIZE - 1,
            onValueChange = {
                sliderValue = it
                onChanged(it.roundToInt())
            },
        )
    }
}