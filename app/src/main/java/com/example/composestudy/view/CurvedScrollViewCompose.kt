package com.example.composestudy.view

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.gestures.FlingBehavior
import androidx.compose.foundation.gestures.ScrollScope
import androidx.compose.foundation.interaction.DragInteraction
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Build
import androidx.compose.material.icons.filled.Create
import androidx.compose.material.icons.filled.DateRange
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.layout.Layout
import androidx.compose.ui.layout.onSizeChanged
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.IntSize
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.composestudy.ui.theme.DarkerButtonBlue
import com.example.composestudy.ui.theme.DeepBlue
import kotlin.math.PI
import kotlin.math.abs
import kotlin.math.cos
import kotlinx.coroutines.launch

@Composable
@Preview
fun CurvedScrollCompose() {
    val items = listOf(
        "Real",
        "Dollar",
        "Euro",
        "Real",
        "Dollar",
        "Euro"
    )

    Column(modifier = Modifier.fillMaxSize()) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .background(DeepBlue),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center
        ) {
            Text(
                text = "Curved ScrollView",
                color = Color.White,
                fontSize = 20.sp,
                fontWeight = FontWeight.Bold
            )

            Box(
                modifier = Modifier
                    .background(Color.DarkGray)
                    .fillMaxSize()
                    .padding(start = 4.dp, end = 4.dp),
                contentAlignment = Alignment.CenterStart
            ) {
                CurvedScrollItem(itemCount = items.size) {
                    Column(modifier = Modifier.wrapContentSize()) {
                        Image(
                            imageVector = when (it) {
                                0 -> Icons.Default.ArrowBack
                                1 -> Icons.Default.Build
                                2 -> Icons.Default.Create
                                3 -> Icons.Default.ArrowBack
                                4 -> Icons.Default.Build
                                5 -> Icons.Default.Create
                                else -> Icons.Default.DateRange
                            },
                            contentDescription = "flags",
                            contentScale = ContentScale.Fit,
                            modifier = Modifier
                                .size(60.dp)
                                .clip(CircleShape)
                                .background(DarkerButtonBlue)
                        )
                        Spacer(modifier = Modifier.padding(5.dp))

                    }
                }
            }
        }
    }
}

@Composable
fun CurvedScrollItem(
    itemCount: Int,
    item: @Composable (Int) -> Unit
) {
    val scrollState = rememberScrollState()

    val size = remember { mutableStateOf(IntSize.Zero) }

    val scope = rememberCoroutineScope()

    val indices = remember { IntArray(itemCount) { 0 } }

    val flingBehavior = object : FlingBehavior {
        override suspend fun ScrollScope.performFling(initialVelocity: Float): Float {
            val value = scrollState.value
            indices.minByOrNull { abs(it - value) }?.let {
                scope.launch {
                    scrollState.animateScrollTo(it)
                }
            }
            return initialVelocity
        }
    }

    Box(modifier = Modifier
        .onSizeChanged {
            size.value = it
        }
    ) {
        Layout(
            content = {
                repeat(itemCount) {
                    item(it)
                }
            },
            modifier = Modifier
                .verticalScroll(
                    state = scrollState,
                    flingBehavior = flingBehavior
                )
                .align(Alignment.CenterStart)

        ) { measurables, constraints ->
            val itemSpacing = 8.dp.roundToPx()
            var contentHeight = (itemCount - 1) * itemSpacing

            val placeables = measurables
                .mapIndexed { index, measurable ->
                    val placeable = measurable.measure(constraints = constraints)
                    contentHeight += if (index == 0 || index == measurables.lastIndex) {
                        placeable.height / 2
                    } else {
                        placeable.height
                    }
                    placeable
                }
            layout(constraints.maxWidth, size.value.height + contentHeight) {
                val startOffset = size.value.height / 2 - placeables[0].height / 2
                var yPosition = startOffset
                val scrollPercent = scrollState.value.toFloat() / scrollState.maxValue

                placeables.forEachIndexed { index, placeable ->
                    val elementRatio = index.toFloat() / placeables.lastIndex
                    val interpolatedValue = cos((scrollPercent - elementRatio) * PI)
                    val indent = interpolatedValue * size.value.width / 2

                    placeable.placeRelativeWithLayer(x = indent.toInt(), y = yPosition) {
                        alpha = interpolatedValue.toFloat()
                    }
                    indices[index] = yPosition - startOffset
                    yPosition += placeable.height + itemSpacing
                }
            }
        }
    }
}