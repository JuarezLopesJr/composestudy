package com.example.composestudy.view

import android.annotation.SuppressLint
import androidx.compose.ui.Modifier
import androidx.compose.ui.composed
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.unit.dp

@SuppressLint("UnnecessaryComposedModifier")
fun Modifier.mediaQuery(
    comparator: DimensionComparator,
    modifier: Modifier
/* this composed after Modifier, it's for give this extension a Composable context */
): Modifier = composed {
    val screenWidth = LocalContext.current.resources.displayMetrics.widthPixels.dp /
            LocalDensity.current.density

    val screenHeight = LocalContext.current.resources.displayMetrics.heightPixels.dp /
            LocalDensity.current.density

    if (comparator.compare(screenWidth, screenHeight)) {
        this.then(modifier)
    } else this
}