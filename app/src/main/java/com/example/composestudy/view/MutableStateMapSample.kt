package com.example.composestudy.view

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateMapOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.tooling.preview.Preview

@Composable
fun NamesAndAges() {
    var name by remember { mutableStateOf("name") }
    var saying by remember { mutableStateOf("saying") }
    val sayings = remember {
        mutableStateMapOf(
            "Caesar" to "Et tu, Brute?",
            "Hamlet" to "To be or not to be",
            "Richard III" to "My kingdom for a horse"
        )
    }

    Column {
        Row {
            BasicTextField(value = name, onValueChange = { name = it })

            BasicTextField(value = saying, onValueChange = { saying = it })

            Button(onClick = { sayings[name] = saying }) {
                Text(text = "Add")
            }

            Button(onClick = { sayings.remove(name) }) {
                Text(text = "Remove")
            }
        }

        Text(text = "Sayings:")
        Column {
            for (entry in sayings) {
                Text(text = "${entry.key} says '${entry.value}'")
            }
        }
    }
}

@Preview
@Composable
fun NamesAndAgesPreview() {
    NamesAndAges()
}