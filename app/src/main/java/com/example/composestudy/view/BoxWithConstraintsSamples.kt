@file:Suppress("unused")

package com.example.composestudy.view

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.size
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp

@Composable
fun BoxWithConstraintsSample() {
    BoxWithConstraints {
        val rectangleHeight = 100.dp

        if (maxHeight < rectangleHeight * 2) {
            Box(
                modifier = Modifier
                    .size(width = 50.dp, height = rectangleHeight)
                    .background(Color.Blue)
            )
        } else {
            Column {
                Box(
                    modifier = Modifier
                        .size(width = 50.dp, height = rectangleHeight)
                        .background(Color.Blue)
                )
                Box(
                    modifier = Modifier
                        .size(width = 50.dp, height = rectangleHeight)
                        .background(Color.Green)
                )
            }
        }
    }
}