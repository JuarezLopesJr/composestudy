package com.example.composestudy.view

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.MagnifierStyle
import androidx.compose.foundation.background
import androidx.compose.foundation.gestures.awaitFirstDown
import androidx.compose.foundation.gestures.calculateCentroid
import androidx.compose.foundation.gestures.calculateCentroidSize
import androidx.compose.foundation.gestures.calculatePan
import androidx.compose.foundation.gestures.calculateRotation
import androidx.compose.foundation.gestures.calculateZoom
import androidx.compose.foundation.gestures.detectDragGestures
import androidx.compose.foundation.gestures.forEachGesture
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.magnifier
import androidx.compose.foundation.progressSemantics
import androidx.compose.foundation.text.selection.DisableSelection
import androidx.compose.foundation.text.selection.SelectionContainer
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.dp
import kotlin.math.roundToInt

@Composable
fun CalculateRotation() {
    var angle by remember { mutableStateOf(0f) }
    Box(
        Modifier
            .graphicsLayer(rotationZ = angle)
            .background(Color.Blue)
            .pointerInput(Unit) {
                forEachGesture {
                    awaitPointerEventScope {
                        awaitFirstDown()
                        do {
                            val event = awaitPointerEvent()
                            val rotation = event.calculateRotation()
                            angle += rotation
                        } while (event.changes.any { it.pressed })
                    }
                }
            }
            .fillMaxSize()
    )
}

@Composable
fun CalculateZoom() {
    var zoom by remember { mutableStateOf(1f) }
    Box(
        Modifier
            .graphicsLayer(scaleX = zoom, scaleY = zoom)
            .background(Color.Blue)
            .pointerInput(Unit) {
                forEachGesture {
                    awaitPointerEventScope {
                        awaitFirstDown()
                        do {
                            val event = awaitPointerEvent()
                            zoom *= event.calculateZoom()
                        } while (event.changes.any { it.pressed })
                    }
                }
            }
            .fillMaxSize()
    )
}

@Composable
fun CalculatePan() {
    val offsetX = remember { mutableStateOf(0f) }
    val offsetY = remember { mutableStateOf(0f) }
    Box(
        Modifier
            .offset { IntOffset(offsetX.value.roundToInt(), offsetY.value.roundToInt()) }
            .graphicsLayer()
            .background(Color.Blue)
            .pointerInput(Unit) {
                forEachGesture {
                    awaitPointerEventScope {
                        awaitFirstDown()
                        do {
                            val event = awaitPointerEvent()
                            val offset = event.calculatePan()
                            offsetX.value += offset.x
                            offsetY.value += offset.y
                        } while (event.changes.any { it.pressed })
                    }
                }
            }
            .fillMaxSize()
    )
}

@Composable
fun CalculateCentroidSize() {
    var centroidSize by remember { mutableStateOf(0f) }
    var position by remember { mutableStateOf(Offset.Zero) }
    Box(
        Modifier
            .drawBehind {
                // Draw a circle where the gesture is
                drawCircle(Color.Blue, centroidSize, center = position)
            }
            .pointerInput(Unit) {
                forEachGesture {
                    awaitPointerEventScope {
                        awaitFirstDown().also {
                            position = it.position
                        }
                        do {
                            val event = awaitPointerEvent()
                            val size = event.calculateCentroidSize()
                            if (size != 0f) {
                                centroidSize = event.calculateCentroidSize()
                            }
                            val centroid = event.calculateCentroid()
                            if (centroid != Offset.Unspecified) {
                                position = centroid
                            }
                        } while (event.changes.any { it.pressed })
                    }
                }
            }
            .fillMaxSize()
    )
}

@ExperimentalFoundationApi
@Composable
fun MagnifierSample() {
    // When the magnifier center position is Unspecified, it is hidden.
    // Hide the magnifier until a drag starts.
    var magnifierCenter by remember { mutableStateOf(Offset.Unspecified) }

    if (!MagnifierStyle.Default.isSupported) {
        Text("Magnifier is not supported on this platform.")
    } else {
        Box(
            Modifier
                .magnifier(
                    sourceCenter = { magnifierCenter },
                    zoom = 2f
                )
                .pointerInput(Unit) {
                    detectDragGestures(
                        // Show the magnifier at the original pointer position.
                        onDragStart = { magnifierCenter = it },
                        // Make the magnifier follow the finger while dragging.
                        onDrag = { _, delta -> magnifierCenter += delta },
                        // Hide the magnifier when the finger lifts.
                        onDragEnd = { magnifierCenter = Offset.Unspecified },
                        onDragCancel = { magnifierCenter = Offset.Unspecified }
                    )
                }
                .drawBehind {
                    // Some concentric circles to zoom in on.
                    for (diameter in 2 until size.maxDimension.toInt() step 10) {
                        drawCircle(
                            color = Color.Black,
                            radius = diameter / 2f,
                            style = Stroke()
                        )
                    }
                }
        )
    }
}

@Composable
fun DeterminateProgressSemanticsSample() {
    val progress = 0.9f // emulate progress from some state
    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Box(
            Modifier
                .progressSemantics(progress)
                .size((progress * 100).dp, 4.dp)
                .background(color = Color.Cyan)
        )
    }
}

@Composable
fun IndeterminateProgressSemanticsSample() {
    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Box(
            Modifier
                .progressSemantics()
                .background(color = Color.Cyan)
        ) {
            Text("Mussum Ipsum is on progress")
        }
    }
}

@Composable
fun SelectionSample() {
    SelectionContainer {
        Column(
            modifier = Modifier.fillMaxSize(),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text("Mussum Ipsum")
            Text("Mussum Ipsum")
            Text("Mussum Ipsum")
        }
    }
}

@Composable
fun DisableSelectionSample() {
    SelectionContainer {
        Column(
            modifier = Modifier.fillMaxSize(),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text("Mussum Ipsum")

            DisableSelection {
                Text("Mussum Ipsum")
                Text("Mussum Ipsum")
            }

            Text("Mussum Ipsum")
        }
    }
}