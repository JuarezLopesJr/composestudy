package com.example.composestudy.view

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.CutCornerShape
import androidx.compose.material.AppBarDefaults
import androidx.compose.material.BottomAppBar
import androidx.compose.material.BottomNavigation
import androidx.compose.material.BottomNavigationItem
import androidx.compose.material.DropdownMenu
import androidx.compose.material.DropdownMenuItem
import androidx.compose.material.ExtendedFloatingActionButton
import androidx.compose.material.FabPosition
import androidx.compose.material.FloatingActionButtonDefaults
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Home
import androidx.compose.material.icons.filled.MoreVert
import androidx.compose.material.icons.filled.Settings
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.google.accompanist.insets.ui.Scaffold

@Composable
@Preview
fun ExtendedFABCompose() {
    val menuExpanded = remember {
        mutableStateOf(false)
    }

    val topBar: @Composable () -> Unit = {
        Surface(color = MaterialTheme.colors.surface) {
            TopAppBar(
                title = {
                    Text(
                        text = "Extended FAB Compose",
                        color = MaterialTheme.colors.onSurface,
                        fontSize = 20.sp
                    )
                },
                navigationIcon = {
                    IconButton(onClick = { /*TODO*/ }) {
                        Icon(
                            imageVector = Icons.Default.ArrowBack,
                            contentDescription = null
                        )
                    }
                },
                actions = {
                    IconButton(onClick = { menuExpanded.value = true }) {
                        Icon(
                            imageVector = Icons.Default.MoreVert,
                            contentDescription = null,
                            tint = Color.White
                        )
                    }

                    Column(
                        modifier = Modifier
                    ) {
                        DropdownMenu(
                            expanded = menuExpanded.value,
                            onDismissRequest = { menuExpanded.value = false },
                            modifier = Modifier.width(200.dp)
                        ) {
                            DropdownMenuItem(onClick = { }) {
                                Text(text = "Sample 1")
                            }
                            DropdownMenuItem(onClick = { }) {
                                Text(text = "Sample 2")
                            }
                            DropdownMenuItem(onClick = { }) {
                                Text(text = "Sample 3")
                            }
                            DropdownMenuItem(onClick = { }) {
                                Text(text = "Sample 4")
                            }
                            DropdownMenuItem(onClick = { }) {
                                Text(text = "Sample 5")
                            }
                        }
                    }
                },
                backgroundColor = MaterialTheme.colors.primary,
                elevation = AppBarDefaults.TopAppBarElevation
            )
        }
    }

    Scaffold(
        topBar = { topBar() },
        content = {},
        floatingActionButton = {
            ExtendedFloatingActionButton(
                text = { Text(text = "Button title") },
                onClick = { },
                icon = {
                    Icon(
                        imageVector = Icons.Default.Add,
                        contentDescription = null
                    )
                },
                elevation = FloatingActionButtonDefaults.elevation()
            )
        },
        isFloatingActionButtonDocked = true,
        floatingActionButtonPosition = FabPosition.Center,
        bottomBar = {
            BottomAppBar(
                cutoutShape = CutCornerShape(50),
                content = {
                    BottomNavigation {
                        BottomNavigationItem(
                            selected = false,
                            onClick = { },
                            icon = {
                                Icon(
                                    imageVector = Icons.Default.Home,
                                    contentDescription = null
                                )
                            },
                            label = { Text(text = "Home") },
                            alwaysShowLabel = false
                        )

                        BottomNavigationItem(
                            selected = false,
                            onClick = { /*TODO*/ },
                            icon = {
                                Icon(
                                    imageVector = Icons.Default.Settings,
                                    contentDescription = null
                                )
                            },
                            label = { Text(text = "Settings") },
                            alwaysShowLabel = false
                        )
                    }
                }
            )
        }
    )
}