package com.example.composestudy.view

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.composestudy.ui.theme.Navy
import com.example.composestudy.ui.theme.OrangeYellow1
import com.example.composestudy.ui.theme.Teal200

data class WindowInfo(
    val screenWidthInfo: WindowType,
    val screenHeightInfo: WindowType,
    val screenWidth: Dp,
    val screenHeight: Dp,
) {
    sealed class WindowType {
        object Compact : WindowType()
        object Medium : WindowType()
        object Expanded : WindowType()
    }
}

@Composable
fun rememberWindowInfo(): WindowInfo {
    val configuration = LocalConfiguration.current

    return WindowInfo(
        screenWidthInfo = when {
            configuration.screenWidthDp < 600 -> WindowInfo.WindowType.Compact
            configuration.screenWidthDp < 840 -> WindowInfo.WindowType.Medium
            else -> WindowInfo.WindowType.Expanded
        },
        screenHeightInfo = when {
            configuration.screenHeightDp < 480 -> WindowInfo.WindowType.Compact
            configuration.screenHeightDp < 900 -> WindowInfo.WindowType.Medium
            else -> WindowInfo.WindowType.Expanded
        },
        screenWidth = configuration.screenWidthDp.dp,
        screenHeight = configuration.screenHeightDp.dp
    )
}

@Composable
fun TabletSizeWindowCompose() {
    val windowInfo = rememberWindowInfo()

    if (windowInfo.screenWidthInfo is WindowInfo.WindowType.Compact) {
        LazyColumn(modifier = Modifier.fillMaxSize()) {
            items(count = 10) {
                Text(
                    text = "Item $it",
                    fontSize = 24.sp,
                    modifier = Modifier
                        .fillMaxWidth()
                        .background(OrangeYellow1)
                        .padding(16.dp)
                )
            }
            /* Second list */
            items(count = 10) {
                Text(
                    text = "Item $it",
                    fontSize = 24.sp,
                    modifier = Modifier
                        .fillMaxWidth()
                        .background(Navy)
                        .padding(16.dp)
                )
            }
        }
    } else {
        Row(modifier = Modifier.fillMaxWidth()) {
            LazyColumn(modifier = Modifier.weight(1f)) {
                items(count = 10) {
                    Text(
                        text = "Item $it",
                        fontSize = 24.sp,
                        modifier = Modifier
                            .fillMaxWidth()
                            .background(Teal200)
                            .padding(16.dp)
                    )
                }
            }

            LazyColumn(modifier = Modifier.weight(1f)) {
                items(count = 10) {
                    Text(
                        text = "Item $it",
                        fontSize = 24.sp,
                        modifier = Modifier
                            .fillMaxWidth()
                            .background(Color.Green)
                            .padding(16.dp)
                    )
                }
            }
        }
    }
}