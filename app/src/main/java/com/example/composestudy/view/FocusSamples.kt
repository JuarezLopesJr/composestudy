@file:Suppress("unused")
@file:OptIn(ExperimentalComposeUiApi::class)

package com.example.composestudy.view

import androidx.compose.foundation.border
import androidx.compose.foundation.focusable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.focus.onFocusChanged
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp

@Composable
fun FocusableSample() {
    var color by remember { mutableStateOf(Color.Black) }

    Box(
        modifier = Modifier
            .border(2.dp, color = color)
            /* onFocusChanged should be added BEFORE the focusable that is being observed */
            .onFocusChanged { color = if (it.isFocused) Color.Green else Color.Black }
            .focusable()
    )
}

@Composable
fun CaptureFocusSample() {
    val focusRequester = remember { FocusRequester() }

    var value by remember { mutableStateOf("berry") }

    var borderColor by remember { mutableStateOf(Color.Yellow) }

    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        TextField(
            value = value,
            onValueChange = {
                value = it.apply {
                    if (length > 5) focusRequester.captureFocus() else focusRequester.freeFocus()
                }
            },
            modifier = Modifier
                .border(2.dp, borderColor)
                .focusRequester(focusRequester)
                .onFocusChanged {
                    borderColor = if (it.isCaptured) Color.Red else Color.Blue
                }
        )
    }
}

@Preview
@Composable
fun SoftwareKeyboardControllerSample() {
    val keyboardController = LocalSoftwareKeyboardController.current

    /* used to ensure a TextField is focused when showing keyboard */
    val focusRequester = remember { FocusRequester() }

    val (text, setText) = remember {
        mutableStateOf("Close keyboard on done ime action (blue)")
    }

    Column(Modifier.padding(16.dp)) {

        BasicTextField(
            value = text,
            onValueChange = setText,
            keyboardOptions = KeyboardOptions(imeAction = ImeAction.Done),
            keyboardActions = KeyboardActions(
                onDone = { keyboardController?.hide() }
            ),
            modifier = Modifier
                .focusRequester(focusRequester)
                .fillMaxWidth()
        )

        Spacer(Modifier.height(16.dp))

        Button(
            onClick = {
                focusRequester.requestFocus()
                keyboardController?.show()
            },
            modifier = Modifier.fillMaxWidth()
        ) {
            Text("Show software keyboard.")
        }
    }
}