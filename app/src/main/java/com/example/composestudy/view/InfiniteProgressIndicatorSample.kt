package com.example.composestudy.view

import androidx.compose.animation.core.RepeatMode
import androidx.compose.animation.core.StartOffset
import androidx.compose.animation.core.StartOffsetType
import androidx.compose.animation.core.animateFloat
import androidx.compose.animation.core.infiniteRepeatable
import androidx.compose.animation.core.rememberInfiniteTransition
import androidx.compose.animation.core.tween
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.unit.dp

/* This is an infinite progress indicator with 3 pulsing dots that grow and shrink */
@Composable
fun InfiniteProgressIndicator() {

    @Composable
    fun Dot(scale: State<Float>) {
        Box(modifier = Modifier
            .padding(5.dp)
            .size(20.dp)
            .graphicsLayer {
                scaleX = scale.value
                scaleY = scale.value
            }
            .background(Color.Green, shape = CircleShape)
        )
    }

    val infiniteTransition = rememberInfiniteTransition()

    val scale1 = infiniteTransition.animateFloat(
        initialValue = 0.2f,
        targetValue = 1f,
        /* no offset for the first animation */
        animationSpec = infiniteRepeatable(
            animation = tween(600),
            repeatMode = RepeatMode.Reverse
        )
    )

    val scale2 = infiniteTransition.animateFloat(
        initialValue = 0.2f,
        targetValue = 1f,
        animationSpec = infiniteRepeatable(
            animation = tween(600),
            repeatMode = RepeatMode.Reverse,
            initialStartOffset = StartOffset(offsetMillis = 150, StartOffsetType.FastForward)
        )
    )

    val scale3 = infiniteTransition.animateFloat(
        initialValue = 0.2f,
        targetValue = 1f,
        animationSpec = infiniteRepeatable(
            animation = tween(600),
            repeatMode = RepeatMode.Reverse,
            initialStartOffset = StartOffset(offsetMillis = 300, StartOffsetType.FastForward)
        )
    )

    Row {
        Dot(scale = scale1)
        Dot(scale = scale2)
        Dot(scale = scale3)
    }
}