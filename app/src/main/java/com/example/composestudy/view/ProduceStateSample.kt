package com.example.composestudy.view

import androidx.compose.foundation.layout.Column
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.produceState
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.emptyFlow
import kotlinx.coroutines.flow.map

data class Person(val name: String = "User")

private sealed class UiState<out T> {
    object Loading : UiState<Nothing>()
    class Data<T>(val data: T) : UiState<T>()
}

class PersonViewModel : ViewModel() {
    val people: Flow<List<Person>> = emptyFlow()
}

/* produceState convert non-Compose state into Compose State */
@Composable
fun ProduceStateSample(viewModel: PersonViewModel) {

    val uiState by
    produceState<UiState<List<Person>>>(
        initialValue = UiState.Loading,
        key1 = viewModel
    ) {
        /* inside a Coroutine scope */
        viewModel.people.map { UiState.Data(it) }.collect { value = it }
    }

    when (val state = uiState) {
        is UiState.Loading -> Text(text = "Loading...")
        is UiState.Data -> Column {
            for (person in state.data) {
                Text(text = "Hi, ${person.name}")
            }
        }
    }
}