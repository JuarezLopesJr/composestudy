package com.example.composestudy.view

import android.widget.NumberPicker
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.viewinterop.AndroidView


@Preview
@Composable
fun NumberPickerCompose() {
    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(text = "Pick a number")
        AndroidNumberPicker(
            onValueChange = { println("onValueChanged: $it") },
            value = 0,
            values = 1..69
        )
    }
}

@Composable
fun AndroidNumberPicker(
    modifier: Modifier = Modifier,
    onValueChange: (Int) -> Unit,
    value: Int,
    values: IntRange,
    displayedValues: Array<String>? = null
) {
    AndroidView(
        modifier = modifier,
        factory = {
            NumberPicker(it).apply {
                minValue = values.first
                maxValue = values.last
                this.displayedValues = displayedValues
                wrapSelectorWheel = false
                this.value = value
                this.setOnValueChangedListener { _, _, newVal ->
                    onValueChange(newVal)
                }
            }
        }
    )
}