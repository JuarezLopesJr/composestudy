package com.example.composestudy.view

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Scaffold
import androidx.compose.material.Snackbar
import androidx.compose.material.SnackbarHostState
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.material.TopAppBar
import androidx.compose.material.rememberScaffoldState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import com.example.composestudy.ui.theme.DarkerButtonBlue
import com.example.composestudy.ui.theme.DeepBlue
import com.example.composestudy.ui.theme.Teal200
import kotlinx.coroutines.launch

@Composable
fun SnackBarCompose() {
    val scaffoldState = rememberScaffoldState(
        snackbarHostState = SnackbarHostState()
    )

    val isVisible = remember {
        mutableStateOf(false)
    }

    val snackBarMessage = remember {
        mutableStateOf("")
    }

    Scaffold(
        scaffoldState = scaffoldState,
        topBar = {
            TopAppBar(
                title = {
                    Text(
                        text = "SnackBar Compose Test",
                        color = Color.White,
                        textAlign = TextAlign.Center,
                        modifier = Modifier.fillMaxWidth()
                    )
                },
                backgroundColor = DeepBlue
            )
        },
        content = { ShowSnackBar(isVisible, snackBarMessage) },
        backgroundColor = Color.DarkGray,
        snackbarHost = {
            if (isVisible.value) {
                Snackbar(
                    action = {
                        TextButton(
                            onClick = {
                                isVisible.value = false
                            }
                        ) {
                            Text(
                                text = "Hide",
                                color = Color.Red
                            )
                        }
                    },
                    content = {
                        Text(text = snackBarMessage.value)
                    },
                    backgroundColor = Teal200
                )
            }
        }
    )
}

@Composable
fun ShowSnackBar(
    visibleState: MutableState<Boolean>,
    snackBarMessage: MutableState<String>
) {
    val scope = rememberCoroutineScope()

    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.Center
    ) {
        Button(
            onClick = {
                scope.launch {
                    visibleState.value = true
                    snackBarMessage.value = "This is a default snackbar"
                }
            },
            colors = ButtonDefaults.buttonColors(DarkerButtonBlue)
        ) {
            Text(
                text = "Show snackbar",
                color = Color.White
            )
        }
    }
}



