/*
package com.example.composestudy.view

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.BadgedBox
import androidx.compose.material.BottomNavigation
import androidx.compose.material.BottomNavigationItem
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState

data class BottomNavItem(
    val name: String,
    val route: String,
    val icon: ImageVector,
    val badgeCount: Int = 0
)

@Composable
fun NavBottomUtil(navController: NavHostController) {
    NavHost(
        navController = navController,
        startDestination = ScreensUtils.HomeScreen.route
    ) {
        composable(route = ScreensUtils.HomeScreen.route) {
            HomeComposable()
        }

        composable(route = ScreensUtils.ChatScreen.route) {
            ChatComposable()
        }

        composable(route = ScreensUtils.SettingsScreen.route) {
            SettingsComposable()
        }
    }
}

@ExperimentalMaterialApi
@Composable
fun BottomNavBar(
    items: List<BottomNavItem>,
    navController: NavController,
    modifier: Modifier = Modifier,
    onItemClick: (BottomNavItem) -> Unit
) {
    */
/* getting the current screen as state to set as selected
     because ONLY STATE CHANGE TRIGGERS RECOMPOSITION *//*

    val backStackEntry = navController.currentBackStackEntryAsState()

    */
/* Compose default widget *//*

    BottomNavigation(
        modifier = modifier,
        backgroundColor = Color.DarkGray,
        elevation = 4.dp
    ) {
        items.forEach {
            */
/* getting the route value name/id *//*

            val selected = it.route == backStackEntry.value?.destination?.route

            */
/* Compose default widget *//*

            BottomNavigationItem(
                selected = selected,
                onClick = { onItemClick(it) },
                selectedContentColor = Color.Green,
                unselectedContentColor = Color.Gray,
                icon = {
                    Column(horizontalAlignment = CenterHorizontally) {
                        if (it.badgeCount > 0) {
                            BadgedBox(badge = {}) {

                                Icon(
                                    imageVector = it.icon,
                                    contentDescription = it.name
                                )


                                Icon(
                                    imageVector = it.icon,
                                    contentDescription = it.name
                                )


                                if (selected) {
                                    Text(
                                        text = it.name,
                                        textAlign = TextAlign.Center,
                                        fontSize = 10.sp
                                    )
                                }
                            }
                        }
                        )

                    }
                }
        }

        @Composable
        fun HomeComposable() {
            Box(
                modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.Center
            ) {
                Text(text = "HOME SCREEN", color = Color.White)
            }
        }

        @Composable
        fun ChatComposable() {
            Box(
                modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.Center
            ) {
                Text(text = "CHAT SCREEN", color = Color.White)
            }
        }

        @Composable
        fun SettingsComposable() {
            Box(
                modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.Center
            ) {
                Text(text = "SETTINGS SCREEN", color = Color.White)
            }
        }*/
