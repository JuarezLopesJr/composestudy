package com.example.composestudy.view.codelabs.layouts

import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.layoutId
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.ConstraintSet
import androidx.constraintlayout.compose.Dimension

@Composable
fun SimpleConstraintCompose() {
    ConstraintLayout {
        /* Create references for the composables to constrain, using destructuring pattern  */
        val (button1, button2, text) = createRefs()

        Button(
            onClick = {},
            /* Assign reference "button" to the Button composable
               and constrain it to the top of the ConstraintLayout  */
            modifier = Modifier.constrainAs(button1) {
                top.linkTo(parent.top, margin = 16.dp)
            }
        ) {
            Text(text = "Button text", color = Color.White)
        }

        Text(
            text = "Text text",
            color = Color.White,
            fontSize = 20.sp,
            /*  Assign reference "text" to the Text composable
                and constrain it to the bottom of the Button composable */
            modifier = Modifier.constrainAs(text) {
                top.linkTo(button1.bottom, margin = 16.dp)
//                centerHorizontallyTo(parent)
                centerAround(button1.end)
            }
        )
        /* create barrier (and other helpers) outside of constrainAs scope,
          linkTo can be used to constrain with guidelines and barriers
          the same way it works for edges of layouts. */
        val barrier = createEndBarrier(button1, text)

        Button(
            onClick = {},
            modifier = Modifier.constrainAs(button2) {
                top.linkTo(parent.top, margin = 16.dp)
                start.linkTo(barrier)
            }
        ) {
            Text("Button 2")
        }
    }
}

/*
@Preview
@Composable
fun SimpleConstraintComposePreview() {
    ComposeStudyTheme(darkTheme = true) {
        SimpleConstraintCompose()
    }
}*/

/* Customizing dimensions */
@Composable
fun LargeConstraintLayout() {
    ConstraintLayout {
        val text = createRef()

        val guideline =
            createGuidelineFromStart(fraction = 0.5f)
        Text(
            "This is a very very very very very very very long text",
            Modifier.constrainAs(text) {
                linkTo(start = guideline, end = parent.end)
                /*this avoid the text to overflow the screen width */
                width = Dimension.preferredWrapContent
                /*Dimensions can be coerced
               width = Dimension.preferredWrapContent.atLeast(100.dp)*/
            }
        )
    }
}

/*@Preview
@Composable
fun LargeConstraintLayoutPreview() {
    ComposeStudyTheme(darkTheme = true) {
        LargeConstraintLayout()
    }
}*/

@Composable
fun DecoupledConstraintLayout() {
    BoxWithConstraints {
        val constraints = if (maxWidth < maxHeight) {
            decoupledConstraints(margin = 16.dp) // Portrait constraints
        } else {
            decoupledConstraints(margin = 32.dp) // Landscape constraints
        }

        ConstraintLayout(constraints) {
            Button(
                onClick = { /* Do something */ },
                modifier = Modifier.layoutId("button")
            ) {
                Text("Button")
            }

            Text("Text", Modifier.layoutId("text"))
        }
    }
}

private fun decoupledConstraints(margin: Dp): ConstraintSet {
    return ConstraintSet {
        val button = createRefFor("button")
        val text = createRefFor("text")

        constrain(button) {
            top.linkTo(parent.top, margin = margin)
        }
        constrain(text) {
            top.linkTo(button.bottom, margin)
        }
    }
}

