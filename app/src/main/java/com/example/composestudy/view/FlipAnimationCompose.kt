package com.example.composestudy.view

import androidx.compose.animation.core.FastOutSlowInEasing
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.animation.core.tween
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.composestudy.R
import com.example.composestudy.ui.theme.DeepBlue
import com.example.composestudy.utils.FlipCard

@ExperimentalMaterialApi
@Composable
fun FlipAnimationCompose() {
    var flipCard by remember {
        mutableStateOf(FlipCard.Forward)
    }

    FlipRotation(
        modifier = Modifier
            .fillMaxWidth(.6f)
            .aspectRatio(1f),
        flipCard = flipCard,
        onClick = {
            flipCard = flipCard.next
        },
        previous = {
            Box(
                modifier = Modifier
                    .fillMaxSize()
                    .background(Color.Yellow),
                contentAlignment = Alignment.Center
            ) {
                Image(
                    painter = painterResource(id = R.drawable.ic_rewind),
                    contentDescription = null,
                    modifier = Modifier.size(120.dp)

                )
            }
        },
        forward = {
            Box(
                modifier = Modifier
                    .fillMaxSize()
                    .background(Color.Green),
                contentAlignment = Alignment.Center
            ) {
                Image(
                    painter = painterResource(id = R.drawable.ic_forward),
                    contentDescription = null,
                    modifier = Modifier.size(120.dp)
                )
            }
        }
    )
}

@ExperimentalMaterialApi
@Composable
fun FlipRotation(
    modifier: Modifier = Modifier,
    flipCard: FlipCard,
    onClick: (FlipCard) -> Unit,
    /* previous and forward will receive the modifier parameter from its call site */
    previous: @Composable () -> Unit = {},
    forward: @Composable () -> Unit = {}
) {
    val rotation = animateFloatAsState(
        targetValue = flipCard.angle,
        animationSpec = tween(
            durationMillis = 400,
            easing = FastOutSlowInEasing
        )
    )

    Column(modifier = Modifier.fillMaxSize()) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .height(55.dp)
                .background(DeepBlue),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center
        ) {
            Text(
                text = "Flip Animation Compose",
                color = Color.White,
                fontSize = 25.sp,
                fontWeight = FontWeight.Bold
            )
        }

        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center,
            modifier = Modifier.fillMaxSize()
        ) {
            /* the modifier from the parameter is passed only to Card
             because it's used in the forward and previous Compose
             parameters, which will be called and modified in the
             FlipAnimationCompose(), call site of FlipRotation(...)
            */
            Card(
                onClick = { onClick(flipCard) },
                modifier = modifier
                    .fillMaxSize()
                    .graphicsLayer {
                        rotationY = rotation.value
                        cameraDistance = 12f * density
                    },
                elevation = 10.dp,
                shape = RoundedCornerShape(10.dp)
            ) {
                if (rotation.value <= 90f) {
                    Box(modifier = Modifier.fillMaxSize()) {
                        forward()
                    }
                } else {
                    Box(
                        modifier = Modifier
                            .fillMaxSize()
                            .graphicsLayer {
                                rotationY = 180f
                            }
                    ) {
                        previous()
                    }
                }
            }
        }
    }
}