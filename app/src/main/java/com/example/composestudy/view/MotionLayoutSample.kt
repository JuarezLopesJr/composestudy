package com.example.composestudy.view

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Slider
import androidx.compose.material.SliderDefaults
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.layout.layoutId
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ExperimentalMotionApi
import androidx.constraintlayout.compose.MotionLayout
import androidx.constraintlayout.compose.MotionScene
import com.example.composestudy.R
import com.example.composestudy.ui.theme.GradientTwo

@ExperimentalMotionApi
@Composable
fun MotionLayoutCompose() {
    var progress by remember { mutableStateOf(0f) }

    Column {
        ProfileHeader(progress = progress)

        Spacer(modifier = Modifier.height(32.dp))

        Slider(
            value = progress,
            onValueChange = { progress = it },
            modifier = Modifier
                .padding(horizontal = 32.dp),
            colors = SliderDefaults.colors(
                thumbColor = GradientTwo,
                activeTrackColor = Color.Green
            )
        )
    }
}

@ExperimentalMotionApi
@Composable
fun ProfileHeader(progress: Float) {
    val context = LocalContext.current


    val motionScene = remember {
        context.resources.openRawResource(R.raw.motion_scene).readBytes().decodeToString()
    }

    MotionLayout(
        motionScene = MotionScene(motionScene),
        progress = progress,
        modifier = Modifier.fillMaxWidth()
    ) {
        val properties =
            motionProperties(id = "profile_picture")

        Box(
            modifier = Modifier
                .fillMaxWidth()
                .background(Color.DarkGray)
                /* layoutId box comes from the motion_scene.json5 file */
                .layoutId("box")
        )

        Image(
            painter = painterResource(id = R.drawable.ic_launcher_foreground),
            contentDescription = null,
            modifier = Modifier
                .clip(CircleShape)
                .border(
                    width = 2.dp,
                    color = properties.value.color("background"),
                    shape = CircleShape
                )
                /* layoutId profile_picture comes from the motion_scene.json5 file */
                .layoutId("profile_picture"),
            colorFilter = ColorFilter.tint(properties.value.color("background"))

        )

        Text(
            text = "Motion Layout Sample",
            fontSize = 24.sp,
            /* layoutId username comes from the motion_scene.json5 file */
            modifier = Modifier.layoutId("username"),
            color = properties.value.color("background")
        )
    }
}