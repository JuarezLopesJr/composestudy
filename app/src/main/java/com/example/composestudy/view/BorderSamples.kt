@file:Suppress("unused")

package com.example.composestudy.view

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.CutCornerShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.TileMode
import androidx.compose.ui.unit.dp
import com.example.composestudy.ui.theme.Teal200
import com.example.composestudy.ui.theme.TextWhite

@Composable
fun BorderWithGradientSample() {
    val gradientBrush = Brush.horizontalGradient(
        colors = listOf(Color.Red, Color.Blue, Color.Green, Teal200),
        startX = 0.0f,
        endX = 500.0f,
        tileMode = TileMode.Repeated
    )

    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            text = "Mussum Ipsum with gradient border",
            color = TextWhite,
            modifier = Modifier
                .border(
                    width = 2.dp,
                    brush = gradientBrush,
                    shape = CircleShape
                )
                .padding(all = 10.dp)
        )
    }
}

@Composable
fun BorderWithDataClassSample() {
    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            text = "Mussum Ipsum with gradient border",
            color = TextWhite,
            modifier = Modifier
                .border(
                    border = BorderStroke(width = 2.dp, color = Color.Blue),
                    shape = CutCornerShape(8.dp)
                )
                .padding(all = 10.dp)
        )
    }
}