package com.example.composestudy.view

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.composestudy.R
import com.example.composestudy.ui.theme.DarkerButtonBlue
import com.example.composestudy.ui.theme.DeepBlue
import com.google.accompanist.swiperefresh.SwipeRefresh
import com.google.accompanist.swiperefresh.rememberSwipeRefreshState
import kotlinx.coroutines.delay

@Composable
fun SwipeToRefreshCompose() {
    Scaffold(
        topBar = {
            TopAppBar(
                title = {
                    Text(
                        text = "Swipe to refresh",
                        modifier = Modifier.fillMaxWidth(),
                        textAlign = TextAlign.Center
                    )
                },
                backgroundColor = DeepBlue
            )
        },
        modifier = Modifier.fillMaxSize()
    ) {
        var isRefreshing by remember {
            mutableStateOf(false)
        }

        LaunchedEffect(key1 = isRefreshing) {
            if (isRefreshing) {
                delay(2000)
                isRefreshing = false
            }
        }

        SwipeRefresh(
            state = rememberSwipeRefreshState(isRefreshing = isRefreshing),
            onRefresh = { isRefreshing = true }
        ) {

            LazyColumn {
                items(count = 20) {
                    Card(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(10.dp, 5.dp, 10.dp, 5.dp)
                            .wrapContentHeight(align = Alignment.CenterVertically),
                        elevation = 5.dp,
                        shape = RoundedCornerShape(8.dp),
                        backgroundColor = DarkerButtonBlue
                    ) {
                        Column(modifier = Modifier.padding(10.dp)) {
                            Column(modifier = Modifier.padding(10.dp)) {
                                Row(verticalAlignment = Alignment.CenterVertically) {
                                    Image(
                                        painter = painterResource(id = R.drawable.ic_launcher_foreground),
                                        contentDescription = null,
                                        contentScale = ContentScale.Crop,
                                        modifier = Modifier
                                            .size(60.dp)
                                            .clip(CircleShape),
                                    )

                                    Spacer(modifier = Modifier.height(10.dp))

                                    Column {
                                        Text(
                                            text = "Sample title",
                                            color = Color.Black,
                                            fontSize = 16.sp,
                                            fontWeight = FontWeight.Bold
                                        )

                                        Spacer(modifier = Modifier.height(5.dp))

                                        Text(
                                            text = "Mussum ipsum, melhor do mundis",
                                            color = Color.White,
                                            fontSize = 12.sp
                                        )
                                    }
                                }

                            }
                        }
                    }
                }
            }
        }
    }
}