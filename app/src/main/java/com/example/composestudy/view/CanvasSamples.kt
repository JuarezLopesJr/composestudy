@file:Suppress("unused")
@file:OptIn(ExperimentalFoundationApi::class)

package com.example.composestudy.view

import androidx.compose.foundation.Canvas
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.size
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.drawscope.inset
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp

@Composable
fun CanvasSample() {
    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Canvas(modifier = Modifier.size(size = 100.dp)) {
            drawRect(color = Color.Magenta)
            inset(10.0f) {
                drawLine(
                    color = Color.Red,
                    start = Offset.Zero,
                    end = Offset(x = size.width, y = size.height),
                    strokeWidth = 5.0f
                )
            }
        }
    }
}

@Preview
@Composable
fun CanvasPieChartSample() {
    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Canvas(
            modifier = Modifier.size(300.dp),
            contentDescription = "Pie chart: 80% apples, 20% bananas (localized string)"
        ) {
            /* Apples 80% */
            drawCircle(color = Color.Red, radius = size.width / 2)

            /* Bananas 20% */
            drawArc(
                color = Color.Yellow,
                startAngle = 0f,
                sweepAngle = 360f * 0.20f,
                useCenter = true,
                topLeft = Offset(x = 0f, y = (size.height - size.width) / 2f),
                size = Size(width = size.width, height = size.height)
            )
        }
    }
}