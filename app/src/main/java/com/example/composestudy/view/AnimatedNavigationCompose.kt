package com.example.composestudy.view

import androidx.compose.animation.AnimatedContentScope
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.core.tween
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import com.example.composestudy.ui.theme.DarkerButtonBlue
import com.example.composestudy.ui.theme.DeepBlue
import com.google.accompanist.navigation.animation.AnimatedNavHost
import com.google.accompanist.navigation.animation.composable
import com.google.accompanist.navigation.animation.rememberAnimatedNavController

const val animationTime = 200

/*@ExperimentalAnimationApi
@Composable
fun NavAnimated() {
    val navController = rememberAnimatedNavController()

    AnimatedNavHost(
        navController = navController,
        startDestination = "screen_one"
    ) {
        *//* Screen One*//*
        composable(
            route = "screen_one",
            enterTransition = { initial, _ ->
                when (initial.destination.route) {
                    "screen_two" -> {
                        slideIntoContainer(
                            AnimatedContentScope.SlideDirection.Left,
                            animationSpec = tween(durationMillis = animationTime)
                        )
                    }
                    else -> null
                }
            },
            exitTransition = { _, target ->
                when (target.destination.route) {
                    "screen_two" -> {
                        slideOutOfContainer(
                            AnimatedContentScope.SlideDirection.Left,
                            animationSpec = tween(durationMillis = animationTime)
                        )
                    }
                    else -> null
                }
            },
            popEnterTransition = { initial, _ ->
                when (initial.destination.route) {
                    "screen_two" -> {
                        slideIntoContainer(
                            AnimatedContentScope.SlideDirection.Right,
                            animationSpec = tween(durationMillis = animationTime)
                        )
                    }
                    else -> null
                }
            },
            popExitTransition = { _, target ->
                when (target.destination.route) {
                    "screen_two" -> {
                        slideOutOfContainer(
                            AnimatedContentScope.SlideDirection.Right,
                            animationSpec = tween(durationMillis = animationTime)
                        )
                    }
                    else -> null
                }
            }
        ) { ScreenOne(navController) }

        *//* Screen Two *//*
        composable(
            route = "screen_two",
            enterTransition = { initial, _ ->
                when (initial.destination.route) {
                    "screen_one" -> {
                        slideIntoContainer(
                            AnimatedContentScope.SlideDirection.Left,
                            animationSpec = tween(durationMillis = animationTime)
                        )
                    }
                    else -> null
                }
            },
            exitTransition = { _, target ->
                when (target.destination.route) {
                    "screen_one" -> {
                        slideOutOfContainer(
                            AnimatedContentScope.SlideDirection.Left,
                            animationSpec = tween(durationMillis = animationTime)
                        )
                    }
                    else -> null
                }
            },
            popEnterTransition = { initial, _ ->
                when (initial.destination.route) {
                    "screen_one" -> {
                        slideIntoContainer(
                            AnimatedContentScope.SlideDirection.Right,
                            animationSpec = tween(durationMillis = animationTime)
                        )
                    }
                    else -> null
                }
            },
            popExitTransition = { _, target ->
                when (target.destination.route) {
                    "screen_one" -> {
                        slideOutOfContainer(
                            AnimatedContentScope.SlideDirection.Right,
                            animationSpec = tween(durationMillis = animationTime)
                        )
                    }
                    else -> null
                }
            }
        ) { ScreenTwo(navController) }
    }
}*/

@Composable
fun ScreenOne(navController: NavHostController) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(DeepBlue),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        NavigateButton(
            "Navigate horizontally",
            Modifier
                .wrapContentWidth()
                .then(Modifier.align(Alignment.CenterHorizontally))
        ) { navController.navigate("screen_two") }
    }
}

@Composable
fun ScreenTwo(navController: NavHostController) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(Color.Green),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        BackButton(navController = navController)
    }
}

@Composable
fun NavigateButton(
    text: String,
    modifier: Modifier = Modifier,
    listener: () -> Unit = {}
) {
    Button(
        onClick = listener,
        modifier = modifier.wrapContentWidth(),
        colors = ButtonDefaults.buttonColors(DarkerButtonBlue)
    ) {
        Text(text = text, color = Color.White)
    }
}

@Composable
fun BackButton(navController: NavController) {
    if (navController.currentBackStackEntry == LocalLifecycleOwner.current &&
        navController.previousBackStackEntry != null
    ) {
        Button(
            onClick = { navController.popBackStack() },
            colors = ButtonDefaults.buttonColors(backgroundColor = Color.LightGray),
            modifier = Modifier.wrapContentWidth()
        ) {
            Text(text = "Go to previous screen", color = Color.Black)
        }
    }
}