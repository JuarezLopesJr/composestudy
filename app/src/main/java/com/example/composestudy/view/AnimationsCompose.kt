@file:OptIn(ExperimentalAnimationApi::class)

package com.example.composestudy.view

import androidx.compose.animation.AnimatedContentScope
import androidx.compose.animation.ContentTransform
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.animateColor
import androidx.compose.animation.core.FastOutLinearInEasing
import androidx.compose.animation.core.LinearEasing
import androidx.compose.animation.core.RepeatMode
import androidx.compose.animation.core.animateDpAsState
import androidx.compose.animation.core.infiniteRepeatable
import androidx.compose.animation.core.keyframes
import androidx.compose.animation.core.rememberInfiniteTransition
import androidx.compose.animation.core.tween
import androidx.compose.animation.with
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.size
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp

private enum class CartState {
    Expanded,
    Collapsed
}

private enum class NestedMenuState { Level1, Level2, Level3 }


@Composable
fun AnimationsComposePreview() {
    var sizeState by remember {
        mutableStateOf(200.dp)
    }

    /* animations utils
    tween(durationMillis = 2000, easing = LinearEasing)
    spring(dampingRatio = Spring.DampingRatioHighBouncy)
    */
    val size by animateDpAsState(
        targetValue = sizeState,
        animationSpec = keyframes {
            durationMillis = 5000
            sizeState at 0 with LinearEasing
            sizeState * 1.5f at 1000 with FastOutLinearInEasing
            sizeState * 2f at 5000
        }
    )

    val infiniteTransition = rememberInfiniteTransition()
    val color by infiniteTransition.animateColor(
        initialValue = Color.Red,
        targetValue = Color.Green,
        animationSpec = infiniteRepeatable(
            tween(durationMillis = 2000),
            repeatMode = RepeatMode.Reverse
        )
    )

    Box(
        modifier = Modifier
            .size(size)
            .background(color),
        contentAlignment = Alignment.Center
    ) {
        Button(onClick = { sizeState += 50.dp }) {
            Text("Animate")
        }
    }

}

@Composable
fun SlideIntoContainerSample() {
    val transitionSpec: AnimatedContentScope<NestedMenuState>.() -> ContentTransform = {
        if (initialState < targetState) {
            slideIntoContainer(towards = AnimatedContentScope.SlideDirection.Left) with
                    slideOutOfContainer(
                        towards = AnimatedContentScope.SlideDirection.Left
                    ) { offsetForFullSlide ->
                        offsetForFullSlide / 2
                    }
        } else {
            slideIntoContainer(
                towards = AnimatedContentScope.SlideDirection.Right
            ) { offsetForFullSlide ->
                offsetForFullSlide / 2
            } with slideOutOfContainer(towards = AnimatedContentScope.SlideDirection.Right)
        }.apply {
            targetContentZIndex = when (targetState) {
                NestedMenuState.Level1 -> 1f
                NestedMenuState.Level2 -> 2f
                NestedMenuState.Level3 -> 3f
            }
        }
    }
}