package com.example.composestudy.view

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.rememberUpdatedState
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.tooling.preview.Preview
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver

@Preview
@Composable
fun LifeCycleScreen() {
    Column(modifier = Modifier.fillMaxSize(), verticalArrangement = Arrangement.Center) {
        Text(text = "Lifecycle screen test")

        LifecycleEvents { event ->
            when (event) {
                Lifecycle.Event.ON_CREATE -> println("$event")
                Lifecycle.Event.ON_START -> println("$event")
                Lifecycle.Event.ON_RESUME -> println("$event")
                Lifecycle.Event.ON_PAUSE -> println("$event")
                Lifecycle.Event.ON_STOP -> println("$event")
                Lifecycle.Event.ON_DESTROY -> println("$event")
                Lifecycle.Event.ON_ANY -> println("$event")
            }
        }
    }
}

@Composable
fun LifecycleEvents(eventHandler: (Lifecycle.Event) -> Unit) {
    val currentEventHandler by rememberUpdatedState(eventHandler)
    val currentLifecycleOwner = LocalLifecycleOwner.current.lifecycle

    DisposableEffect(currentLifecycleOwner) {
        val observer = LifecycleEventObserver { _, event ->
            currentEventHandler(event)
        }

        currentLifecycleOwner.addObserver(observer)

        onDispose {
            currentLifecycleOwner.removeObserver(observer)
        }
    }
}