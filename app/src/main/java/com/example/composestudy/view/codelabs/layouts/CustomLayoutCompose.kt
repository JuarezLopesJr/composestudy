package com.example.composestudy.view.codelabs.layouts

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.AlignmentLine
import androidx.compose.ui.layout.FirstBaseline
import androidx.compose.ui.layout.Layout
import androidx.compose.ui.layout.layout
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.example.composestudy.ui.theme.ComposeStudyTheme

/* this is for a single custom Composable */
fun Modifier.firstBaselineToTop(
    firstBaselineToTop: Dp
) =
    layout { measurable, constraints ->
        /* measuring the Composable, this measurement it's made only once  */
        val childCompose = measurable.measure(constraints)

        /* checking if the Composable has a first baseline */
        check(childCompose[FirstBaseline] != AlignmentLine.Unspecified)
        val firstBaseline = childCompose[FirstBaseline]

        /* calculating its size (width and height) */
        val childComposeY = firstBaselineToTop.roundToPx() - firstBaseline
        val height = childCompose.height + childComposeY

        /* position the Composable on the screen by calling
            childCompose(placeable).placeRelative(x, y).
           if i don't call placeRelative, the composable won't be visible. */
        layout(childCompose.width, height) {
            childCompose.placeRelative(0, childComposeY)
        }
    }

@Composable
@Preview
fun UIWithCustomBaselinePreview() {
    ComposeStudyTheme {
        Column(modifier = Modifier.firstBaselineToTop(32.dp)) {
            Text(
                text = "Custom baseline",
                color = Color.White,
//                modifier = Modifier.firstBaselineToTop(32.dp)
            )
        }
    }
}

@Composable
@Preview
fun UIWithDefaultBaselinePreview() {
    ComposeStudyTheme {
        Text(
            text = "Default baseline",
            color = Color.White,
            modifier = Modifier.padding(top = 32.dp)
        )
    }
}

/* for a group of custom Composables */
/* basic structure, minimum parameters are modifier and content */
@Composable
fun CustomLayoutComposable(
    modifier: Modifier = Modifier,
    content: @Composable () -> Unit
) {
    Layout(
        modifier = modifier,
        content = content
    ) { measurables, constraints ->
        /* List of measured composables children */
        val childCompose = measurables.map {
            /* measure each child */
            it.measure(constraints)
        }
        /* keeping track of the y coordinate */
        var yPosition = 0

        /* parent custom layout size */
        layout(constraints.maxWidth, constraints.maxHeight) {
            /* placing children in the parent layout */
            childCompose.forEach {
                it.placeRelative(x = 0, y = yPosition)

                yPosition += it.height
            }
        }
    }
}

@Preview
@Composable
fun CustomLayoutComposablePreview(modifier: Modifier = Modifier) {
    ComposeStudyTheme(darkTheme = true) {
        CustomLayoutComposable(modifier.padding(16.dp)) {
            Text("MyOwnColumn")
            Text("places items")
            Text("vertically.")
            Text("We've done it by hand!")
        }
    }
}