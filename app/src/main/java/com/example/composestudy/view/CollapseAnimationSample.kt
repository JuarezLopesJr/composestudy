package com.example.composestudy.view

import androidx.compose.animation.AnimatedContent
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.SizeTransform
import androidx.compose.animation.core.FastOutSlowInEasing
import androidx.compose.animation.core.RepeatMode
import androidx.compose.animation.core.animateFloat
import androidx.compose.animation.core.infiniteRepeatable
import androidx.compose.animation.core.rememberInfiniteTransition
import androidx.compose.animation.core.tween
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.with
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Icon
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Android
import androidx.compose.material.icons.outlined.Backspace
import androidx.compose.material.icons.outlined.Camera
import androidx.compose.material.icons.outlined.GraphicEq
import androidx.compose.material.icons.outlined.Keyboard
import androidx.compose.material.icons.outlined.Share
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.unit.dp
import com.example.composestudy.ui.theme.Navy

@ExperimentalAnimationApi
@Composable
fun CollapseTapAnimationCompose(modifier: Modifier = Modifier) {
    var expanded by remember { mutableStateOf(false) }

    AnimatedContent(
        targetState = expanded,
        transitionSpec = {
            if (targetState) {
                fadeIn(animationSpec = tween(700)) with
                        fadeOut(animationSpec = tween(300))
            } else {
                fadeIn(animationSpec = tween(300)) with
                        fadeOut(animationSpec = tween(500))
            }.using(
                SizeTransform(
                    sizeAnimationSpec = { _, _ ->
                        tween(600)
                    }
                )
            )
        },
        contentAlignment = Alignment.Center,
        modifier = modifier
            .background(
                color = Navy.copy(alpha = 0.7f),
                shape = RoundedCornerShape(percent = 100)
            )
            .clickable(
                interactionSource = remember { MutableInteractionSource() },
                indication = null,
                onClick = { expanded = !expanded }
            )
    ) { targetExpanded ->
        if (!targetExpanded) {
            /* initial circle button animation */
            Box(contentAlignment = Alignment.Center) {
                val infiniteTransition = rememberInfiniteTransition()

                val size by infiniteTransition.animateFloat(
                    initialValue = 40f,
                    targetValue = 72f,
                    animationSpec = infiniteRepeatable(
                        animation = tween(3000, easing = FastOutSlowInEasing),
                        repeatMode = RepeatMode.Restart
                    )
                )

                val animatedAlpha by infiniteTransition.animateFloat(
                    initialValue = 1f,
                    targetValue = 0f,
                    animationSpec = infiniteRepeatable(
                        animation = tween(3000),
                        repeatMode = RepeatMode.Restart
                    )
                )

                Box(
                    modifier = Modifier
                        .size(size.dp)
                        .graphicsLayer {
                            alpha = animatedAlpha
                        }
                        .background(
                            brush = Brush.radialGradient(
                                0.4f to Color.Green.copy(alpha = 0f),
                                0.6f to Color.Green.copy(alpha = 0.5f),
                                1.0f to Color.Green
                            ),
                            shape = CircleShape
                        )
                )

                Box(modifier = Modifier.padding(12.dp)) {
                    Icon(
                        imageVector = Icons.Default.Android,
                        contentDescription = null
                    )
                }
            }
        } else {
            val iconList = listOf(
                Icons.Outlined.Backspace,
                Icons.Outlined.Keyboard,
                Icons.Outlined.GraphicEq,
                Icons.Outlined.Camera,
                Icons.Outlined.Share
            )

            Row(
                modifier = Modifier.padding(
                    horizontal = 36.dp,
                    vertical = 18.dp
                ),
                horizontalArrangement = Arrangement.spacedBy(20.dp)
            ) {
                iconList.forEach {
                    Icon(
                        imageVector = it,
                        contentDescription = null,
                        modifier = Modifier.size(36.dp),
                        tint = Color.White
                    )
                }
            }
        }
    }
}