package com.example.composestudy.view

import android.view.MotionEvent
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.clipToBounds
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Rect
import androidx.compose.ui.graphics.ClipOp
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.graphics.Path
import androidx.compose.ui.graphics.drawscope.clipPath
import androidx.compose.ui.input.pointer.pointerInteropFilter
import androidx.compose.ui.res.imageResource
import androidx.compose.ui.unit.IntSize
import androidx.compose.ui.unit.dp
import com.example.composestudy.R

data class DraggedPath(
    val path: Path,
    val width: Float = 50f
)

@ExperimentalComposeUiApi
@Composable
fun ScratchCanvasCard() {
    val overLayImage =
        ImageBitmap.imageResource(id = R.drawable.music)

    val baseImage =
        ImageBitmap.imageResource(id = R.drawable.eagle_nebula)

    val currentPathState = remember {
        mutableStateOf(DraggedPath(path = Path()))
    }

    val movedOffsetState = remember {
        mutableStateOf<Offset?>(null)
    }

    Box(
        modifier = Modifier.fillMaxSize()
    ) {
        ScratchCanvasAnimation(
            modifier = Modifier.align(Alignment.Center),
            overLayImage = overLayImage,
            baseImage = baseImage,
            movedOffset = movedOffsetState.value,
            onMovedOffset = { x, y ->
                movedOffsetState.value = Offset(x, y)
            },
            currentPath = currentPathState.value.path,
            currentPathThickness = currentPathState.value.width
        )
    }
}

@ExperimentalComposeUiApi
@Composable
fun ScratchCanvasAnimation(
    modifier: Modifier = Modifier,
    overLayImage: ImageBitmap,
    baseImage: ImageBitmap,
    movedOffset: Offset?,
    onMovedOffset: (Float, Float) -> Unit,
    currentPath: Path,
    currentPathThickness: Float
) {
    Canvas(
        modifier = modifier
            .size(220.dp)
            .clipToBounds()
            .clip(RoundedCornerShape(16.dp))
            .border(width = 1.dp, color = Color.LightGray, shape = RoundedCornerShape(16.dp))
            .pointerInteropFilter {
                when (it.action) {
                    MotionEvent.ACTION_DOWN -> {
                        currentPath.moveTo(it.x, it.y)
                    }

                    MotionEvent.ACTION_MOVE -> {
                        onMovedOffset(it.x, it.y)
                    }
                }
                true
            }
    ) {
        val canvasWidth = size.width.toInt()

        val canvasHeight = size.height.toInt()

        val imageSize = IntSize(canvasWidth, canvasHeight)

        drawImage(
            image = overLayImage,
            dstSize = imageSize
        )

        movedOffset?.let {
            currentPath.addOval(oval = Rect(it, currentPathThickness))
        }

        clipPath(
            path = currentPath,
            clipOp = ClipOp.Intersect
        ) {
            drawImage(
                image = baseImage,
                dstSize = imageSize
            )
        }
    }
}