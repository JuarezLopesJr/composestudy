package com.example.composestudy.view

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.requiredWidth
import androidx.compose.material.Badge
import androidx.compose.material.BadgedBox
import androidx.compose.material.BottomAppBar
import androidx.compose.material.BottomNavigation
import androidx.compose.material.BottomNavigationItem
import androidx.compose.material.Button
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material.icons.filled.Home
import androidx.compose.material.icons.filled.Navigation
import androidx.compose.material.icons.filled.Notifications
import androidx.compose.material.icons.filled.Person
import androidx.compose.material.icons.filled.Search
import androidx.compose.material.icons.filled.Settings
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.composestudy.ui.theme.LightGreen3

@Composable
fun BadgeCompose() {
    val badgeCount = remember {
        mutableStateOf(0)
    }

    Scaffold(
        topBar = {
            TopAppBar(
                title = {
                    Text(
                        text = "Badge Count Compose",
                        textAlign = TextAlign.Center,
                        modifier = Modifier.fillMaxWidth()
                    )
                },
                backgroundColor = MaterialTheme.colors.primary,
                actions = {
                    if (badgeCount.value == 0) {
                        Icon(
                            imageVector = Icons.Default.Notifications,
                            contentDescription = null
                        )
                    } else {
                        BadgedBox(
                            badge = {
                                Badge(backgroundColor = LightGreen3) {
                                    Text(text = "${badgeCount.value}")
                                }
                            }
                        ) {
                            Icon(
                                imageVector = Icons.Default.Notifications,
                                contentDescription = null
                            )
                        }
                    }
                    Spacer(modifier = Modifier.requiredWidth(12.dp))
                },
                navigationIcon = {
                    IconButton(onClick = { }) {
                        Icon(
                            imageVector = Icons.Default.Navigation,
                            contentDescription = null
                        )
                    }
                }

            )
        },
        content = {
            Box(
                modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.Center
            ) {
                Button(onClick = { badgeCount.value++ }) {
                    Text(text = "Update badge count")
                }
            }
        },
        backgroundColor = Color.Gray,
        bottomBar = {
            BottomAppBar(
                content = {
                    BottomNavigation {
                        BottomNavigationItem(
                            selected = false,
                            onClick = { },
                            icon = {
                                BadgedBox(
                                    badge = {
                                        Badge {
                                            Text(text = "${badgeCount.value}")
                                        }
                                    }
                                ) {
                                    Icon(
                                        imageVector = Icons.Default.Home,
                                        contentDescription = null
                                    )
                                }
                            },
                            label = { Text(text = "Home") },
                            alwaysShowLabel = false
                        )

                        BottomNavigationItem(
                            selected = false,
                            onClick = { },
                            icon = {
                                BadgedBox(
                                    badge = {
                                        Badge {
                                            Text(text = "${badgeCount.value}")
                                        }
                                    }
                                ) {
                                    Icon(
                                        imageVector = Icons.Default.Favorite,
                                        contentDescription = null
                                    )
                                }
                            },
                            label = { Text(text = "Favorite") },
                            alwaysShowLabel = false
                        )

                        BottomNavigationItem(
                            selected = false,
                            onClick = { },
                            icon = {
                                BadgedBox(
                                    badge = {
                                        Badge {
                                            Text(text = "${badgeCount.value}")
                                        }
                                    }
                                ) {
                                    Icon(
                                        imageVector = Icons.Default.Search,
                                        contentDescription = null
                                    )
                                }
                            },
                            label = { Text(text = "Search") },
                            alwaysShowLabel = false
                        )

                        BottomNavigationItem(
                            selected = false,
                            onClick = { },
                            icon = {
                                BadgedBox(
                                    badge = {
                                        Badge {
                                            Text(text = "${badgeCount.value}")
                                        }
                                    }
                                ) {
                                    Icon(
                                        imageVector = Icons.Default.Settings,
                                        contentDescription = null
                                    )
                                }
                            },
                            label = { Text(text = "Settings") },
                            alwaysShowLabel = false
                        )

                        BottomNavigationItem(
                            selected = false,
                            onClick = { },
                            icon = {
                                BadgedBox(
                                    badge = {
                                        Badge {
                                            Text(text = "${badgeCount.value}")
                                        }
                                    }
                                ) {
                                    Icon(
                                        imageVector = Icons.Default.Person,
                                        contentDescription = null
                                    )
                                }
                            },
                            label = { Text(text = "Person") },
                            alwaysShowLabel = false
                        )
                    }
                }
            )
        }
    )
}
        