package com.example.composestudy.view

import androidx.compose.material.Button
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.rememberScaffoldState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.produceState
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.tooling.preview.Preview
import kotlinx.coroutines.delay

@Preview(showSystemUi = true)
@Composable
fun SideEffectsAndHandlersPreview() {
    val scaffoldState = rememberScaffoldState()
    val coroutineScope = rememberCoroutineScope()

    Scaffold(scaffoldState = scaffoldState) {
        /* ALWAYS USE DELEGATE TO PASS/DEFINE STATE
        var counter by remember {
            mutableStateOf(0)
        }*/

        val counter = produceState(initialValue = 0) {
            /* this produceState, set an initial value then produces another
             value from this block of code, triggering an recomposition
              use it with LaunchedEffect coroutine scope */
            delay(3000L)
            value = 5 // new value/state produced
        }.value

        if (counter % 5 == 0 && counter > 0) {
            /* Simpler way, the downside is that snackbar will fire multiple times
            coroutineScope.launch {
                scaffoldState
                    .snackbarHostState
                    .showSnackbar("$counter is divisible by 5")
            }*/

            /* LaunchedEffect will basically cancel and restart the coroutine, that way snackbar
             won't be enqueued */
            LaunchedEffect(key1 = scaffoldState.snackbarHostState) {
                scaffoldState
                    .snackbarHostState
                    .showSnackbar("$counter is divisible by 5")
            }
        }

        /* while using produceState, i can't call counter++ in onClick callback */
        Button(onClick = { }) {
            Text(text = "Clicked $counter times")

        }
    }
}