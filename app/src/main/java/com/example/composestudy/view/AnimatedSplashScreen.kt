package com.example.composestudy.view

import android.annotation.SuppressLint
import android.view.animation.OvershootInterpolator
import androidx.compose.animation.core.Animatable
import androidx.compose.animation.core.tween
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.scale
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.navigation.NavController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.composestudy.R
import kotlinx.coroutines.delay

sealed class ScreensUtils(val route: String) {
    object MainScreenFromSplash : Screens("mainScreen")

    @SuppressLint("CustomSplashScreen")
    object SplashScreen : Screens("splashScreen")

    object HomeScreen : ScreensUtils("home")
    object ChatScreen : ScreensUtils("chat")
    object SettingsScreen : ScreensUtils("settings")
}

@Composable
fun NavigationUtil() {
    val navController = rememberNavController()

    NavHost(
        navController = navController,
        startDestination = ScreensUtils.SplashScreen.route
    ) {
        composable(route = ScreensUtils.SplashScreen.route) {
            SplashScreenCompose(navController = navController)
        }

        composable(route = ScreensUtils.MainScreenFromSplash.route) {
            MainScreenCompose()
        }
    }
}

@Composable
fun SplashScreenCompose(navController: NavController) {
    val scale = remember {
        Animatable(0f)
    }

    LaunchedEffect(key1 = true) {
        scale.animateTo(
            targetValue = 3f,
            animationSpec = tween(
                durationMillis = 600,
                easing = {
                    OvershootInterpolator(3f).getInterpolation(it)
                }
            )
        )
        delay(3000L)
        navController.navigate(ScreensUtils.MainScreenFromSplash.route)
    }

    Box(
        contentAlignment = Alignment.Center,
        modifier = Modifier.fillMaxSize()
    ) {
        Image(
            painter = painterResource(id = R.drawable.ic_launcher_foreground),
            contentDescription = "Splash screen image",
            modifier = Modifier.scale(scale.value)
        )
    }
}

@Composable
fun MainScreenCompose() {
    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            text = "Main Screen",
            modifier = Modifier.fillMaxSize(),
            textAlign = TextAlign.Center,
            style = TextStyle(Color.White)
        )
    }
}