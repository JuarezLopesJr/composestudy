package com.example.composestudy.view

import androidx.compose.foundation.LocalIndication
import androidx.compose.foundation.clickable
import androidx.compose.foundation.indication
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.requiredHeight
import androidx.compose.material.Text
import androidx.compose.material.ripple.rememberRipple
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.example.composestudy.ui.theme.TextWhite

@Composable
fun IndicationCompose() {
    val interactionSource = remember { MutableInteractionSource() }

    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            text = "Click me and my neighbour will indicate as well!",
            modifier = Modifier
                /* clickable will dispatch events using MutableInteractionSource and show ripple */
                .clickable(
                    interactionSource = interactionSource,
                    indication = rememberRipple()
                ) {}
                .padding(all = 10.dp),
            color = TextWhite
        )

        Spacer(modifier = Modifier.requiredHeight(10.dp))

        Text(
            text = "I'm neighbour and I indicate when you click the other one",
            modifier = Modifier
                /* this element doesn't have a click, but will show default indication from the
                   CompositionLocal as it accepts the same MutableInteractionSource */
                .indication(
                    interactionSource = interactionSource,
                    indication = LocalIndication.current
                )
                .padding(all = 10.dp),
            color = TextWhite
        )
    }
}