package com.example.composestudy.di

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.core.DataStoreFactory
import androidx.datastore.core.handlers.ReplaceFileCorruptionHandler
import androidx.datastore.dataStoreFile
import com.example.composestudy.UserStore
import com.example.composestudy.repository.preferences.proto_data_store.ProtoUserRepository
import com.example.composestudy.repository.preferences.proto_data_store.ProtoUserRepositoryImpl
import com.example.composestudy.repository.preferences.proto_data_store.UserStoreSerializer
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob

private const val DATA_STORE_FILE_NAME = "user_store.pb"

@Module
@InstallIn(SingletonComponent::class)
object ProtoDataStoreModule {

    @Provides
    @Singleton
    fun provideProtoDataStore(
        @ApplicationContext context: Context
    ): DataStore<UserStore> {
        return DataStoreFactory.create(
            serializer = UserStoreSerializer,
            produceFile = { context.dataStoreFile(DATA_STORE_FILE_NAME) },
            corruptionHandler = ReplaceFileCorruptionHandler(
                produceNewData = { UserStore.getDefaultInstance() }
            ),
            scope = CoroutineScope(Dispatchers.IO + SupervisorJob())
        )
    }

    @Provides
    @Singleton
    fun provideProtoUserRepository(userStore: DataStore<UserStore>): ProtoUserRepository {
        return ProtoUserRepositoryImpl(userStore)
    }
}