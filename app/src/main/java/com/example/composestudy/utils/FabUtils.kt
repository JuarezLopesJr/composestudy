package com.example.composestudy.utils

import androidx.annotation.DrawableRes
import androidx.compose.material.MaterialTheme
import androidx.compose.material.contentColorFor
import androidx.compose.runtime.Composable
import androidx.compose.runtime.Immutable
import androidx.compose.runtime.Stable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.graphics.Color

@Immutable
interface FabIcon {
    @Stable
    val iconRes: Int

    @Stable
    val iconRotate: Float?

}

@Immutable
interface FabOption {
    @Stable
    val iconTint: Color

    @Stable
    val backgroundTint: Color

    @Stable
    val showLabel: Boolean
}

/* setting/ override methods in the class primary constructor */
private class FabIconImpl(
    override val iconRes: Int,
    override val iconRotate: Float?
) : FabIcon

private class FabOptionImpl(
    override val iconTint: Color,
    override val backgroundTint: Color,
    override val showLabel: Boolean
) : FabOption

sealed class MultiFabState {
    object Collapsed : MultiFabState()
    object Expanded : MultiFabState()

    fun isExpanded() = this == Expanded

    fun toggleValue() = if (isExpanded()) Collapsed else Expanded
}

data class MultiFabItem(
    val id: Int,
    @DrawableRes val iconRes: Int,
    val label: String = ""
)

fun fabIcon(@DrawableRes iconRes: Int, iconRotate: Float? = null): FabIcon =
    FabIconImpl(iconRes, iconRotate)

@Composable
fun fabOption(
    backgroundTint: Color = MaterialTheme.colors.primary,
    iconTint: Color = contentColorFor(backgroundColor = backgroundTint),
    showLabel: Boolean = false
): FabOption = FabOptionImpl(iconTint, backgroundTint, showLabel)

@Composable
fun rememberMultiFabState() = remember {
    mutableStateOf<MultiFabState>(MultiFabState.Collapsed)
}