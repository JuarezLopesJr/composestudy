package com.example.composestudy.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.composestudy.repository.preferences.proto_data_store.ProtoUserRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

@HiltViewModel
class ProtoUserViewModel @Inject constructor(
    userRepository: ProtoUserRepository
) : ViewModel() {

    val userRepo = userRepository

        val getState = viewModelScope.launch {
            userRepository.getUserLoggedState().collect()
        }

}