package com.example.composestudy.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.example.composestudy.repository.model.UserData
import com.example.composestudy.utils.UserSourcePagination
import kotlinx.coroutines.flow.Flow

class UserViewModel : ViewModel() {
    val user: Flow<PagingData<UserData>> = Pager(PagingConfig(pageSize = 6)) {
        UserSourcePagination()
    }.flow.cachedIn(viewModelScope)
}