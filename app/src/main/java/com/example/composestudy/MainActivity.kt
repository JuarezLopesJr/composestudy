package com.example.composestudy

import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.annotation.RequiresApi
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Card
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.toMutableStateList
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.CenterVertically
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ExperimentalMotionApi
import androidx.datastore.core.handlers.ReplaceFileCorruptionHandler
import androidx.datastore.dataStore
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.lifecycleScope
import androidx.paging.PagingData
import androidx.paging.compose.LazyPagingItems
import androidx.paging.compose.collectAsLazyPagingItems
import androidx.paging.compose.items
import coil.compose.rememberImagePainter
import com.example.composestudy.repository.model.UserData
import com.example.composestudy.repository.preferences.proto_data_store.AppSettings
import com.example.composestudy.repository.preferences.proto_data_store.AppSettingsSerializer
import com.example.composestudy.repository.preferences.proto_data_store.Language
import com.example.composestudy.ui.theme.ComposeStudyTheme
import com.example.composestudy.ui.theme.DeepBlue
import com.example.composestudy.ui.theme.GradientOne
import com.example.composestudy.ui.theme.GradientTwo
import com.example.composestudy.ui.theme.OrangeYellow1
import com.example.composestudy.ui.theme.Teal200
import com.example.composestudy.view.AnimatedShuffleVerticalGrid
import com.example.composestudy.view.ExoplayerCompose
import com.example.composestudy.view.InfiniteProgressIndicator
import com.example.composestudy.view.StrikeThruIcon
import com.example.composestudy.view.StrikeThruImage
import com.example.composestudy.view.SwipeToDismissListItems
import com.example.composestudy.viewmodel.ProtoUserViewModel
import com.example.composestudy.viewmodel.UserViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

@ExperimentalComposeUiApi
@ExperimentalAnimationApi
@ExperimentalMaterialApi
@Suppress("PrivatePropertyName")
@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    /*private val userViewModel by viewModels<UserViewModel>()
    private val userProtoUserViewModel by viewModels<ProtoUserViewModel>()*/

    /* private val DATA_STORE_FILE_NAME = "user_store.pb"

     private val Context.userDataStore: DataStore<UserStore> by dataStore(
         fileName = DATA_STORE_FILE_NAME,
         serializer = UserStoreSerializer,
         *//* This handler is invoked if a CorruptionException is thrown by the serializer
          when the data cannot be de-serialized *//*
        corruptionHandler = ReplaceFileCorruptionHandler(
            produceNewData = { UserStore.getDefaultInstance() }
        )
    )
    var userRepo: ProtoUserRepository? = null*/

   /* private val userRepo = userProtoUserViewModel.userRepo

    private val Context.dataStore by
    dataStore(
        fileName = "app_settings.json",
        serializer = AppSettingsSerializer,
        corruptionHandler = ReplaceFileCorruptionHandler(
            produceNewData = { AppSettings() }
        )
    )

    private suspend fun setLanguage(language: Language) {
        dataStore.updateData {
            it.copy(
                language = language
            )
        }
    }*/

    @SuppressLint("UnusedMaterialScaffoldPaddingParameter")
    @OptIn(ExperimentalMotionApi::class)
    @RequiresApi(Build.VERSION_CODES.Q)
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContent {
            ComposeStudyTheme {
                Scaffold(
                    topBar = {
                        TopAppBar(
                            title = {
                                Text(
                                    modifier = Modifier.fillMaxWidth(),
                                    text = "Animation test",
                                    textAlign = TextAlign.Center
                                )
                            }
                        )
                    }
                ) {
//                    AnimatedShuffleVerticalGrid()
                    ExoplayerCompose()
                }
                /* Box(
                     contentAlignment = Alignment.Center,
                     modifier = Modifier
                         .fillMaxSize()
                         .background(Color(0xFF101010))
                 ) {
                     Row(
                         horizontalArrangement = Arrangement.Center,
                         verticalAlignment = Alignment.CenterVertically,
                         modifier = Modifier
                             .border(
                                 1.dp,
                                 Color.Green,
                                 RoundedCornerShape(10.dp)
                             )
                             .padding(30.dp)
                     ) {
                         var volume by remember {
                             mutableStateOf(0f)
                         }

                         val barCount = 20
                         MusicKnob(modifier = Modifier.size(100.dp)) {
                             volume = it
                         }

                         Spacer(modifier = Modifier.width(20.dp))

                         VolumeBar(
                             modifier = Modifier
                                 .fillMaxWidth()
                                 .height(30.dp),
                             activeBars = (barCount * volume).roundToInt(),
                             barCount = barCount
                         )
                     }
                 }*/
                /*Surface(
                    color = Color(0xFF101010),
                    modifier = Modifier.fillMaxSize()
                ) {
                    Box(
                        contentAlignment = Alignment.Center
                    ) {
                        Timer(modifier = Modifier.size(200.dp))
                    }
                }*/

                /* MediaQuery(Dimensions.Width lessThan 400.dp) {
                     Text(
                         text = "shown only below a width of 400dp",
                         style = TextStyle(Color.White)
                     )
                 }*/
                /* another way to implement the MediaQuery() */
                /*Text(
                    text = "shown only below a width of 600dp",
                    modifier = Modifier
                        .background(Color.Green)
                        .mediaQuery(
                            Dimensions.Width lessThan 600.dp,
                            modifier = Modifier.fillMaxSize()
                        ),
                    style = TextStyle(Color.White)
                )*/
                /*NavigationUtil()*/
                /*val navController = rememberNavController()

                Scaffold(bottomBar = {
                    BottomNavBar(
                        items = listOf(
                            BottomNavItem(
                                name = "Home",
                                route = ScreensUtils.HomeScreen.route,
                                icon = Icons.Default.Home
                            ),
                            BottomNavItem(
                                name = "Chat",
                                route = ScreensUtils.ChatScreen.route,
                                icon = Icons.Default.Notifications,
                                badgeCount = 69
                            ),
                            BottomNavItem(
                                name = "Settings",
                                route = ScreensUtils.SettingsScreen.route,
                                icon = Icons.Default.Settings
                            )
                        ),
                        navController = navController,
                        onItemClick = {
                            navController.navigate(it.route)
                        }
                    )
                }) {
                    NavBottomUtil(navController = navController)
                }*/
                /* DropDownCompose(
                     text = "Settings",
                     modifier = Modifier.padding(15.dp)
                 ) {
                     Text(
                         text = "revealed",
                         modifier = Modifier
                             .fillMaxWidth()
                             .height(100.dp)
                             .background(Color.Green)
                     )
                 }*/
//                HomeMeditation()
//                ProfileScreen()
//                ItemSelectable()
//                SwipeToRefreshCompose()
//                SwipeableContent()
//                BottomBarWithFAB()
//                ImagePickerCompose()
//                UserList(userViewModel)
//                SnackBarCompose()
//                CurvedScrollCompose()
//                CollapseToolbar()
//                PlayerScreen()
//                NavAnimated()
//                GesturesCompose()
//                FlipAnimationCompose()
//                MultipleCheckBoxesCompose()
//                ScrollToPositionCompose()
//                ExpandFabButton()
//                BottomNoteCompose()
//                BadgeCompose()
                /*DragDropList(
                    items = ReorderItem,
                    onMove = { fromIndex, toIndex -> ReorderItem.move(fromIndex, toIndex) }
                )*/
//                AnimateIncrementDecrementSample()
//                AnimateContentModifier()
//                MotionLayoutCompose()
//                SelectAnimation()
                /*Column(
                    modifier = Modifier.fillMaxSize(),
                    verticalArrangement = Arrangement.Center,
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    StrikeThruIcon()
                    Spacer(modifier = Modifier.height(48.dp))
                    StrikeThruImage()
                    Spacer(modifier = Modifier.height(48.dp))
                    InfiniteProgressIndicator()
                }
            }*/
        }
    }

    /*@Composable
    private fun UserList(userViewModel: UserViewModel) {
        UserListDetail(userViewModel.user)
    }

    @Composable
    private fun UserListDetail(user: Flow<PagingData<UserData>>) {
        val userListItem: LazyPagingItems<UserData> = user.collectAsLazyPagingItems()

        Column(modifier = Modifier.fillMaxSize()) {
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .background(DeepBlue)
                    .padding(15.dp),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Center
            ) {
                Text(
                    text = "Paging Compose Test",
                    fontSize = 20.sp,
                    color = Color.White,
                    fontWeight = FontWeight.Bold
                )
            }

            Spacer(modifier = Modifier.height(10.dp))

            LazyColumn {
                items(userListItem) { item ->
                    item?.let {
                        UserLists(it)
                    }
                }
            }

        }

    }

    @Composable
    private fun UserLists(userData: UserData) {
        Card(
            modifier = Modifier
                .padding(10.dp, 5.dp, 10.dp, 5.dp)
                .fillMaxWidth(),
            shape = RoundedCornerShape(10.dp),
            elevation = 12.dp
        ) {
            Row(
                modifier = Modifier
                    .padding(10.dp)
                    .clip(RoundedCornerShape(4.dp))
                    .background(Color.LightGray)
            ) {
                Surface(
                    modifier = Modifier
                        .size(100.dp)
                        .clip(RoundedCornerShape(12.dp))
                ) {
                    // Using Coil
                    Image(
                        painter = rememberImagePainter(
                            data = userData.avatar,
                            builder = {
                                crossfade(true)
                            }
                        ),
                        contentDescription = null,
                        modifier = Modifier.clip(RoundedCornerShape(10.dp)),
                        contentScale = ContentScale.Crop
                    )
                }

                Column(
                    modifier = Modifier
                        .padding(start = 15.dp)
                        .align(CenterVertically)
                ) {
                    Text(
                        text = "Name: ${userData.first_name} ${userData.last_name}",
                        fontWeight = FontWeight.Bold,
                        fontSize = 20.sp,
                        color = Color.Black
                    )

                    Text(
                        text = "Email: ${userData.email}",
                        fontWeight = FontWeight.Normal,
                        fontSize = 14.sp,
                        color = Color.DarkGray,
                        modifier = Modifier.padding(20.dp)
                    )
                }
            }

        }
    }*/


    /*@SuppressLint("CoroutineCreationDuringComposition")
    @Composable
    fun ProtoBuffer(
        viewModel: ProtoUserViewModel = hiltViewModel()
    ) {
        val stateFlag = remember {
            mutableStateOf(viewModel.getState.isCompleted)
        }
        lifecycleScope.launch {
            userRepo.getUserLoggedState().collect { state ->
                withContext(Dispatchers.Main) {
                    stateFlag.value = state
                }
            }
        }

        Column(
            modifier = Modifier
                .fillMaxSize()
                .background(
                    if (stateFlag.value) Teal200 else GradientOne
                ),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center
        ) {
            Text(
                text = "User logged in State ${stateFlag.value}",
                fontSize = 25.sp,
                color = MaterialTheme.colors.onSurface
            )

            Spacer(modifier = Modifier.height(20.dp))

            Button(
                onClick = {
                    lifecycleScope.launch {
                        userRepo?.saveUserLoggedInState(true)
                    }
                },
                modifier = Modifier
                    .fillMaxWidth(0.7f)
                    .clip(RoundedCornerShape(10.dp)),
                colors = ButtonDefaults.buttonColors(GradientTwo)
            ) {
                Text(
                    text = "Log in",
                    fontSize = 16.sp,
                    color = MaterialTheme.colors.onSurface,
                    modifier = Modifier.padding(5.dp)
                )
            }

            Spacer(modifier = Modifier.height(20.dp))

            Button(
                onClick = {
                    lifecycleScope.launch {
                        userRepo?.saveUserLoggedInState(false)
                    }
                },
                modifier = Modifier
                    .fillMaxWidth(0.7f)
                    .clip(RoundedCornerShape(10.dp)),
                colors = ButtonDefaults.buttonColors(OrangeYellow1)
            ) {
                Text(
                    text = "Log out",
                    fontSize = 16.sp,
                    color = MaterialTheme.colors.onSurface,
                    modifier = Modifier.padding(5.dp)
                )
            }

        }*/
    }
}

val ReorderItem = listOf(
    "Item 1",
    "Item 2",
    "Item 3",
    "Item 4",
    "Item 5",
    "Item 6",
    "Item 7",
    "Item 8",
    "Item 9",
    "Item 10",
    "Item 11",
    "Item 12",
    "Item 13",
    "Item 14",
    "Item 15",
    "Item 16",
    "Item 17",
    "Item 18",
    "Item 19",
    "Item 20"
).toMutableStateList()