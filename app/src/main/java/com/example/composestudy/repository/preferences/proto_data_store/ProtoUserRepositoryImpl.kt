package com.example.composestudy.repository.preferences.proto_data_store

import androidx.datastore.core.DataStore
import com.example.composestudy.UserStore
import java.io.IOException
import javax.inject.Inject
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.map

class ProtoUserRepositoryImpl @Inject constructor(
    private val protoDataStore: DataStore<UserStore>
) : ProtoUserRepository {
    override suspend fun saveUserLoggedInState(state: Boolean) {
        protoDataStore.updateData { store ->
            store.toBuilder()
                .setIsLoggedIn(state)
                .build()
        }
    }

    override suspend fun getUserLoggedState(): Flow<Boolean> {
        return protoDataStore.data
            .catch { exception ->
                if (exception is IOException) {
                    emit(UserStore.getDefaultInstance())
                } else {
                    throw exception
                }
            }.map { protoBuilder ->
                protoBuilder.isLoggedIn
            }
    }
}