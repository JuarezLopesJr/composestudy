@file:Suppress("BlockingMethodInNonBlockingContext")

package com.example.composestudy.repository.preferences.proto_data_store

import androidx.datastore.core.CorruptionException
import androidx.datastore.core.Serializer
import com.example.composestudy.UserStore
import com.google.protobuf.InvalidProtocolBufferException
import java.io.InputStream
import java.io.OutputStream

object UserStoreSerializer : Serializer<UserStore> {
    override val defaultValue: UserStore = UserStore.getDefaultInstance()

    override suspend fun readFrom(input: InputStream): UserStore {
        try {
            return UserStore.parseFrom(input)
        } catch (exception: InvalidProtocolBufferException) {
            throw CorruptionException("Cannot read protobuf", exception)
        }
    }

    override suspend fun writeTo(t: UserStore, output: OutputStream) = t.writeTo(output)
}