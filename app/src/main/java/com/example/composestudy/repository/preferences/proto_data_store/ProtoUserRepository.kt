package com.example.composestudy.repository.preferences.proto_data_store

import kotlinx.coroutines.flow.Flow

interface ProtoUserRepository {
    suspend fun saveUserLoggedInState(state: Boolean)
    suspend fun getUserLoggedState(): Flow<Boolean>
}