package com.example.composestudy.repository.preferences.proto_data_store

import kotlinx.serialization.Serializable

enum class Language {
    ENGLISH,
    GERMAN,
    SPANISH
}

@Serializable
data class AppSettings(
    val language: Language = Language.ENGLISH
)

