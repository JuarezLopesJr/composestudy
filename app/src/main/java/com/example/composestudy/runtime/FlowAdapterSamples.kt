@file:Suppress("unused")

package com.example.composestudy.runtime

import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.StateFlow

@Composable
fun FlowWithInitialSample(flow: Flow<String>) {
    val value by flow.collectAsState(initial = "initial")
    Text(text = "Value is $value")
}

@Composable
fun StateFlowSample(stateFlow: StateFlow<String>) {
    val value by stateFlow.collectAsState()
    Text(text = "Value is $value")
}