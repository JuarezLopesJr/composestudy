@file:Suppress(
    "unused", "UNUSED_PARAMETER", "UNUSED_VARIABLE", "LocalVariableName", "CanBeVal",
    "ComposableNaming"
)

package com.example.composestudy.runtime

import androidx.compose.foundation.layout.Row
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.State
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.key
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue

@Composable
fun observeUserSample() {
    @Composable
    fun observeUser(userId: Int): User? {
        val user = remember(userId) {
            mutableStateOf<User?>(null)
        }

        DisposableEffect(key1 = userId) {
            /* set the observer */
            val subscription = UserAPI.subscribeToUser(userId = userId) {
                user.value = it
            }
            /* dispose the observer ALWAYS */
            onDispose {
                subscription.unsubscribe()
            }
        }
        return user.value
    }
}

@Composable
fun TwoInputKeySample() {
    for (element in elements) {
        val selected by key(element.id, parentId) {
            remember { mutableStateOf(false) }
        }
        ListItem(item = element, selected = selected)
    }
}

@Composable
fun SimpleStateSample() {
    var count by remember { mutableStateOf(0) }

    Text(text = "Clicked $count times")
    Button(onClick = { count += 1 }) { // could be count++
        Text(text = "Click")
    }
}

@Composable
fun DestructuredStateSample() {
    val (count, setCount) = remember { mutableStateOf(0) }

    Text(text = "Clicked $count times")
    Button(onClick = { setCount(count + 1) }) {
        Text(text = "Click")
    }
}

@Composable
fun DelegatedReadOnlySample() {
    /* Composable function that manages a subscription to a data source, returning it as State */
    @Composable
    fun observeSampleData(): State<String> = TODO()

    /* Subscription is managed here, but currentValue is not read yet */
    val currentValue by observeSampleData()

    Row {
        /* This scope will recompose when currentValue changes */
        Text(text = "Data: $currentValue")
    }
}

@Composable
fun DerivedStateSample() {
    @Composable
    fun CountDisplay(count: State<Int>) {
        Text(text = "Count: ${count.value}")
    }

    @Composable
    fun Example() {
        var a by remember { mutableStateOf(0) }
        var b by remember { mutableStateOf(0) }

        val sum = remember { derivedStateOf { a + b } }
        /* Changing either a or b will cause CountDisplay to recompose but not trigger Example
            to recompose */
        CountDisplay(count = sum)
    }
}

data class User(
    val id: Int = 0,
    val name: String = "User"
)

private class Subscription {
    fun unsubscribe() {}
}

private object UserAPI {
    fun subscribeToUser(userId: Int, user: (User) -> Unit): Subscription {
        return Subscription()
    }
}

private val elements = listOf<Element>()

private class Element(val id: Int)

@Composable
private fun ListItem(item: Any, selected: Boolean) {
}

private const val parentId = 0