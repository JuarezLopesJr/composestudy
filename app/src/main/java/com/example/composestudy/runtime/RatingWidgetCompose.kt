package com.example.composestudy.runtime

import android.util.Log
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.size
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Rect
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Path
import androidx.compose.ui.graphics.drawscope.clipPath
import androidx.compose.ui.graphics.drawscope.scale
import androidx.compose.ui.graphics.drawscope.translate
import androidx.compose.ui.graphics.vector.PathParser
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.example.composestudy.R
import com.example.composestudy.ui.theme.OrangeYellow1

const val FILLED = "filled_star"
const val HALF = "half_filled_star"
const val EMPTY = "empty_star"

enum class StarType {
    FILLED_STAR,
    HALF_FILLED_STAR,
    EMPTY_STAR
}

@Composable
fun RatingWidgetCompose(
    modifier: Modifier = Modifier,
    rating: Double,
    scaleFactor: Float = 3f,
    spaceBetween: Dp = 8.dp
) {
    val starPathString = stringResource(id = R.string.star_path)

    val starPath = remember { PathParser().parsePathString(starPathString).toPath() }

    val starPathBounds = remember { starPath.getBounds() }

    val result = calculateStars(rating = rating)

    Row(
        modifier = modifier,
        horizontalArrangement = Arrangement.spacedBy(spaceBetween)
    ) {
        result[FILLED]?.let {
            repeat(it) {
                StarRating(
                    starPath = starPath,
                    starPathBounds = starPathBounds,
                    scaleFactor = scaleFactor,
                    starType = StarType.FILLED_STAR
                )
            }
        }

        result[HALF]?.let {
            repeat(it) {
                StarRating(
                    starPath = starPath,
                    starPathBounds = starPathBounds,
                    scaleFactor = scaleFactor,
                    starType = StarType.HALF_FILLED_STAR
                )
            }
        }

        result[EMPTY]?.let {
            repeat(it) {
                StarRating(
                    starPath = starPath,
                    starPathBounds = starPathBounds,
                    scaleFactor = scaleFactor,
                    starType = StarType.EMPTY_STAR
                )
            }
        }
    }
}

@Composable
fun StarRating(
    starPath: Path,
    starPathBounds: Rect,
    scaleFactor: Float,
    starType: StarType
) {
    Canvas(modifier = Modifier.size(24.dp)) {
        val canvasSize = size

        /* Controlling the size of the star by changing scaleFactor */
        scale(scale = scaleFactor) {
            val pathWidth = starPathBounds.width
            val pathHeight = starPathBounds.height

            /* Calculation to center the star (rectangle) in the canvas */
            val left = (canvasSize.width / 2f) - (pathWidth / 1.7f)
            val top = (canvasSize.height / 2f) - (pathHeight / 1.7f)

            translate(left = left, top = top) {
                when (starType) {
                    StarType.FILLED_STAR -> {
                        drawPath(
                            path = starPath,
                            color = OrangeYellow1
                        )
                    }

                    StarType.HALF_FILLED_STAR -> {
                        drawPath(
                            path = starPath,
                            color = Color.LightGray.copy(alpha = 0.5f)
                        )
                        /* height must be aware of the scaleFactor, which is dynamic
                            clipPath() get the half of the star */
                        clipPath(path = starPath) {
                            drawRect(
                                color = OrangeYellow1,
                                size = Size(
                                    width = starPathBounds.maxDimension / 1.7f,
                                    height = starPathBounds.maxDimension * scaleFactor
                                )
                            )
                        }
                    }

                    StarType.EMPTY_STAR -> {
                        drawPath(
                            path = starPath,
                            color = Color.LightGray.copy(alpha = 0.5f)
                        )
                    }
                }
            }
        }
    }
}

@Composable
fun calculateStars(rating: Double): Map<String, Int> {
    val maxStars by remember { mutableStateOf(5) }

    var filledStars by remember { mutableStateOf(0) }

    var halfFilledStars by remember { mutableStateOf(0) }

    var emptyStars by remember { mutableStateOf(0) }

    LaunchedEffect(key1 = rating) {
        val (firstNumber, lastNumber) = rating.toString()
            .split(".")
            .map { it.toInt() }

        if (firstNumber in 0..5 && lastNumber in 0..9) {
            filledStars = firstNumber

            when (lastNumber) {
                in 1..5 -> halfFilledStars++
                in 6..9 -> filledStars++
            }

            if (firstNumber == 5 && lastNumber > 0) {
                emptyStars = 5
                filledStars = 0
                halfFilledStars = 0
            }
        } else {
            Log.d("Rating widget", "Invalid rating value...how did you get here ?!")
        }
    }

    emptyStars = maxStars - (filledStars + halfFilledStars)

    return mapOf(
        FILLED to filledStars,
        HALF to halfFilledStars,
        EMPTY to emptyStars
    )
}