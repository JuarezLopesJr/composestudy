@file:Suppress("unused", "UNUSED_PARAMETER", "UNUSED_VARIABLE")

package com.example.composestudy.runtime

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.movableContentOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.sp
import com.example.composestudy.ui.theme.OrangeYellow1

@Composable
fun ItemView(userId: Int) {
}
typealias Item = Int

@Composable
fun ShowMovable() {
    MovableContentColumnRowSample(vertical = true) {
        repeat(6) {
            Text(text = "Item $it", fontSize = 20.sp, color = OrangeYellow1)
        }
    }
}

@Composable
fun MovableContentColumnRowSample(
    vertical: Boolean,
    content: @Composable () -> Unit
) {
    val movableContent = remember(content as Any) {
        movableContentOf(content)
    }

    if (vertical) {
        Column(
            modifier = Modifier.fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center
        ) {
            movableContent()
        }
    } else {
        Row(
            modifier = Modifier.fillMaxSize(),
            horizontalArrangement = Arrangement.Center,
            verticalAlignment = Alignment.CenterVertically
        ) {
            movableContent()
        }
    }
}
