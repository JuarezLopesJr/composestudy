@file:Suppress("unused", "UNUSED_PARAMETER", "UNUSED_VARIABLE")

package com.example.composestudy.runtime

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier

@Composable
fun LoginScreen() {
    val (userName, setUserName) = remember { mutableStateOf("") }
    val (password, setPassword) = remember { mutableStateOf("") }

    /* simulating Retrofit Api call to login
    fun login() = Api.login(userName, password) */

    Column(
        modifier = Modifier.fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        BasicTextField(
            value = userName,
            onValueChange = { setUserName(it) }
        )

        BasicTextField(
            value = password,
            onValueChange = { setPassword(it) }
        )

        Button(onClick = { /*TODO*/ }) {
            Text(text = "Login")
        }
    }
}